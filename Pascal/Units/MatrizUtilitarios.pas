Unit MatrizUtilitarios;

Interface 
Type Matriz = Array [1..100, 1..100] Of Integer;

Procedure LeMatriz(Var Matrix:Matriz; M, N:Integer);
Procedure ImprimeMatriz(Var Matrix:Matriz; M, N:Integer);

Implementation
Procedure LeMatriz(Var Matrix:Matriz; M, N:Integer);
Var I, J:LongInt;
Begin
    For I:= 1 To M Do
        For J:= 1 To N Do
            Read(Matrix[I, J]);
End;

Procedure ImprimeMatriz(Var Matrix:Matriz; M, N:Integer);
Var I, J:LongInt;
Begin
    For I:= 1 To M Do
    Begin
        For J:= 1 To N Do
            Write(Matrix[I, J],' ');
        Writeln();
    End;

End;

End.

