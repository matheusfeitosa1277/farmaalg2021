{*
Um número inteiro positivo é dito triangular se seu valor é o produto de três números naturais consecutivos. 
Por exemplo, o número 120 é triangular porque 120 = 4 x 5 x 6 . 

Faça um programa Pascal que leia do teclado um número inteiro positivo n e verifique se ele é triangular ou 
não. Se for, imprima 1 e se não for, imprima 0.
*}

(* Conceitos parece Divergir da definicao da Wikipedia ... *)
(* De qualquer forma, Vamos resolver o problema acima *)

Program L4E1;
Var N, Aux, P:LongInt;

Begin
    Aux:= 1; (* Segundo Numero Triangular *)
    P:= 6;
    Read(N);

    If (N And 1 = 1) Or (N Mod 3 <> 0) Then Writeln(0) (*Todo Numero Triangular Eh par e Divisivel por 3 *)
    Else Begin
        While N < P Do
        Begin
            P:= P Div Aux;
            P:= P * (Aux + 3); 
            Aux:= Aux + 1;
        End;

        If P = N 
            Then Writeln(1)
            Else Writeln(0);

    End;
End.

