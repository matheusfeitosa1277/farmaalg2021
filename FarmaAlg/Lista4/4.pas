{*
A definição de uma série de pares Valor-Quadrado em Matemática é dada pela presença de elementos 
numéricos, organizados em sequências dois-a-dois, em que cada número é seguido de seu quadrado 
exatamente uma vez (veja os exemplos abaixo). 

Faça um programa Pascal que leia do teclado uma sequência com uma quantidade arbitrária de valores inteiros 
positivos. A sequência termina com o valor 0, que serve para indicar o final da entrada de dados e não deverá 
ser processado. 
O programa deve determinar e imprimir se a série é ou não do tipo Valor-Quadrado imprimindo 1 caso seja e 0 
caso não seja.
*}

Program L4E4;
Var N, Aux:LongInt;
Label Saida;

Begin
    Read(N);
    While N <> 0 Do
    Begin
        Read(Aux);
        If (N*N <> Aux) Then Begin
            Writeln(0);
            Goto Saida;
        End;
        Read(N);
    End;

    Writeln(1);
    Saida:
End.

