{*
Dadas as populações Pa e Pb de duas cidades A e B (no ano atual) e suas respectivas taxas de crescimento anual 
Ta e Tb, faça um programa Pascal que que receba 4 números reais como entrada (Pa, Pb, Ta, Tb) representando 
estas informações e determine se a população da cidade de menor população ultrapassará a de maior 
população e se sim, imprima em quantos anos que isto ocorrerá. Caso contrário, imprima 0.
*}

(* Funcao exponencial *)

Program L4E2;
Var Pa, Pb, Ta, Tb:Real;
    Ano:LongInt;

(* 1 Opcao -> Forca Bruta *)
Function Anos(Pa, Pb, Ta, Tb:Real):LongInt; (* Pa Ultrapassa Pb *)
Var Tempo:LongInt;

Begin
    Tempo:= 0;
    Repeat
        Tempo:= Tempo + 1;
        Pa:= Pa + Pa * Ta;
        Pb:= Pb + Pb * Tb;
    Until Pa > Pb;

    Anos:= Tempo;

End;

Begin
    Read(Pa, Pb, Ta, Tb);
    
    If (Pa < Pb) And (Ta > Tb) Then Ano:= Anos(Pa, Pb, Ta, Tb) (* Pa Ultrapassa *)
    Else If (Pb < Pa) And (Tb > Ta) Then Ano:= Anos(Pb, Pa, Tb, Ta)(* Pb Ultrapassa *)
    Else Ano:= 0;

    Writeln(Ano);
    
End.

