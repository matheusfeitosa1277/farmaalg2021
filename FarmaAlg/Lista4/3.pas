{*
Um inteiro positivo N é considerado perfeito se o mesmo for igual a soma de seus divisores positivos diferentes
de N.

Exemplo:6 é perfeito pois 1 + 2 + 3 =6 e 1, 2 e 3 são todos os divisores positivos de 6 e que são diferentes de 6.

Faça um programa Pascal que leia um número inteiro positivo K e mostre os K primeiros números que são 
perfeitos.
*}

(* Bem interresante este problema, vamos ver como q a gente vai resolver isso... *)
(* 2 Opcoes - Forca Bruta ou Euclid-Euler theorem *)
(* Sieve Of Atkin + Even Perfect number - 2^(p-1)*(2^p - 1), onde p Eh primo *)

Program L4E3;
Var K, Aux, N, I, Perfeitos:LongInt;

Begin
    Perfeitos:= 0; N:= 1;
    Read(K);

    (* 1 Opcao - Forca Bruta *)
    While Perfeitos < K Do 
    Begin
        Aux:= 0; 
        N:= N + 1; 
        
        For I:= N-1 DownTo 1 Do (* Todos os divisores de N *)
            If N mod I = 0 Then Aux:= Aux + I;

        If Aux = N Then Begin
            Perfeitos:= Perfeitos + 1;
            Write(Aux, ' ');
        End;
    End;
    Writeln();

End.

