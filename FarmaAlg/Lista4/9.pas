{*
Faça um programa Pascal que leia do teclado uma sequência de N > 0 números inteiros quaisquer. Para cada 
valor lido, se ele for positivo, imprima os primeiros 10 múltiplos dele. Assuma que a sequência tenha pelo 
menos 1 número e que ela termina com 0. O zero serve para indicar o final da entrada de dados e não deverá ser 
processado.
*}

Program L4E9;
Var N, I:LongInt;

Begin
    Read(N);
    While N <> 0 Do
    Begin
        For I:= 1 To 10 Do Write(N*I, ' ');
        Read(N);
    End;
End.

