{*
Faça um programa Pascal que leia do teclado uma sequência de números inteiros até que seja lido um número 
que seja o dobro ou a metade do anteriormente lido. O programa deve imprimir na saída a quantidade de 
números lidos, a soma dos números lidos e os dois valores que forçaram a parada do programa.
*}

(* Buffer Circular Parece uma solucao mais elegante *)

Program L4E5;
Var N1, N2, Total, Soma:LongInt;

Begin

    Read(N1, N2); Soma:= N1 + N2;
    Total:= 2;
    While (N1 <> 2*N2) And (N1 <> N2 / 2) Do
    Begin
        N1:= N2;
        Read(N2);
        Soma:= Soma + N2;
        Total:= Total + 1;
    End;
    Write(Total, ' ', Soma, ' ', N1, ' ', N2);

End.

