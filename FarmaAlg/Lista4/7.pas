{*
Faça um programa Pascal que leia do teclado três números inteiros positivos: i, j e k. Se j for múltiplo de i e k
for múltiplo de j, o programa deve imprimir a soma dos três. Se os três valores forem consecutivos na ordem lida,
o programa deve imprimi-los na ordem decrescente
Em qualquer outra situação, o programa deve calcular e imprimir a média aritmética simples dos três valores. 
*}

Program L4E7;
Var I, J, K:LongInt;

Begin
    Read(I, J, K);
    
    If (J Mod I = 0) And (K Mod J = 0) Then Writeln(I+J+K)
    Else If (I + 1 = J) And (J + 1 = K) Then Writeln(K, ' ', J, ' ', I, ' ')
    Else Writeln((I+J+K) Div 3);
End.

