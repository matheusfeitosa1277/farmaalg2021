{*
Faça um programa Pascal que leia do teclado um número inteiro maior que 1 e verifique se este número é 
primo ou não.
*}

(* Sieve of Atkin parece uma boa *)

Program L4E6;
Var N, I:LongInt;
Label Saida;

Begin
    Read(N);

    If (N And 1 = 1) Then (* Se For Impar *)
    Begin
        I:= 3; (* Primeiro Impar Primo *) 
        While I <= Sqrt(N) Do
        Begin
            If N Mod I = 0 Then Goto Saida;
            I:= I + 2;
        End;
        Writeln('SIM');
    End
    Else If N = 2
        Then Writeln('SIM')
        Else Saida: Writeln('NAO');
End.

