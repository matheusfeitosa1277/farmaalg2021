{*
Um número de Keith é um número inteiro, superior a 9, tal que os seus algarismos, ao começarem uma 
sequência de Fibonacci (formada por somas de 2-em-2 números, ou de 3-em-3, ou de 4-em-4, e assim por 
diante), alcançam posteriormente o referido número. Um exemplo é 47, porque a sequência de Fibonacci que 
começa com 4 e 7 (4, 7, 11, 18, 29, 47) alcança o 47. 
Outro exemplo, mas que possui três algarismos, é 197: 1+9+7=17, 9+7+17=33, 7+17+33=57, 17+33+57=107, 
33+57+107=197.

Faça um programa Pascal que receba um número inteiro positivo de até 2 algarismos do teclado e imprima 1 
caso o número seja de Keith e 0 caso contrário.
*}

Program L4E10;
Var N, F1, F2, Aux:LongInt;
Label Saida;

Begin
    Read(N);
    F1:= N Div 10; F2:= N Mod 10;
    If N < 10 Then Goto Saida;

    While N > F2 Do
    Begin
        Aux:= F1;
        F1:= F2;
        F2:= F2 + Aux;
    End;

    If F2 = N 
        Then Writeln(1)
        Else Saida: Writeln(0);


End.

