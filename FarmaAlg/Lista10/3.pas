{*
Dizemos que uma matriz quadrada inteira é um quadrado mágico se a soma dos elementos de cada linha, a 
soma dos elementos de cada coluna e a soma dos elementos das diagonais principal e secundária são todos 
iguais. Exemplo:

    8 0 7
    4 5 6
    3 10 2

é um quadrado mágico pois 8+0+7 = 4+5+6 = 3+10+2 = 8+4+3 = 0+5+10 = 7+6+2 = 8+5+2 = 3+5+7 = 15.

Crie um programa em Free Pascal que leia um valor n representando o tamanho da matriz e leia uma matrix A(n
x n) que representa o quadrado e informe se a matriz é um quadrado mágico.
*}

Program L10E3;
Uses MatrizUtilitarios In '../../Pascal/Units/MatrizUtilitarios.pas';
Var N:LongInt;
    M:Matriz;

(* Implementacao Ingenua *)
Function QuadradoMagico(Var M:Matriz; N:LongInt):Boolean;
Var I, J, Linha, Coluna, ConstMag:LongInt;
Begin
    QuadradoMagico:= True;

    ConstMag:= 0;
    For J:= 1 To N Do
        ConstMag += M[1][J];

    (* Checando Linha e Colunas *)
    For I:= 1 To N Do
    Begin
        Linha:= 0; Coluna:= 0;
        For J:= 1 To N Do
        Begin
            Linha += M[I][J]; 
            Coluna += M[J][I];
        End;

        If (Linha <> ConstMag) Or (Coluna <> ConstMag) Then
        Begin
            QuadradoMagico:= False;
            Exit;
        End;
    End;

    (* Checando Diagnoais *)
    Linha:= 0; Coluna:= 0; J:= N;
    For I:= 1 To N Do
    Begin
        Linha += M[I][I];
        Coluna += M[I][J];
        J -= 1;
    End;

    If (Linha <> ConstMag) Or (Coluna <> ConstMag) Then
    Begin
        QuadradoMagico:= False;
        Exit;
    End;

End;

Begin
    Read(N);
    LeMatriz(M, N, N);

    If QuadradoMagico(M, N)
        Then Writeln('sim')
        Else Writeln('nao');
End.

