{*
Um jogo de palavras cruzadas pode ser representado por uma matriz A(n × m) onde cada 
posição da matriz corresponde a um quadrado do jogo, sendo que 0 indica um quadrado 
em branco e -1 indica um quadrado preto. Colocar as numerações de início de palavras 
horizontais e/ou verticais nos quadrados correspondentes (substituindo os zeros),
considerando que uma palavra deve ter pelo menos duas letras.

PS: A imagem abaixo representa visualmente a matriz de entrada do exemplo. As palavras 
sempre são adicionadas da esquerda para a direita (Horizontal) ou de cima para baixo 
(Vertical).
*}

Program L10E2;
Uses MatrizUtilitarios In '../../Pascal/Units/MatrizUtilitarios.pas';
Var I, J, K, L, Palavra:LongInt;
    M:Matriz;
Begin
    Palavra:= 0;
    Read(I, J);
    LeMatriz(M, I, J);

    For K:= 1 To I Do
    Begin
        For L:= 1 To J Do
        Begin
            If M[K][L] = 0 Then Begin
                (* Nova Palavra  *)
                If ((K < I) And (M[K+1][L] = 0) And ((K = 1) Or (M[K-1][L] = -1 ))) Or  (* Cima | Baixo *)
                   ((L < J) And (M[K][L+1] = 0) And ((L = 1) Or (M[K][L-1] = -1 )))     (* Esq  | Dir   *)

                Then Begin
                    Palavra += 1;    
                    M[K][L]:= Palavra;
                End;

                
            End;
        End;
    End;

    ImprimeMatriz(M, I, J);

End.

