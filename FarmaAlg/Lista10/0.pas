{*
Uma matriz D(8 × 8) pode representar a posiçao atual de um jogo de damas, sendo que 0 indica uma casa vazia,
1 indica uma casa ocupada por uma peça branca e -1 indica uma casa ocupada por uma peça preta. Supondo
que as peças pretas estão se movendo no sentido crescente das linhas da matriz D, determinar as posições das 
peças pretas que:

    podem tomar peças brancas;
    podem mover-se sem tomar peças brancas;
    não podem se mover.

A prioridade na ação é sempre tomar uma peça, ou seja, em uma situação na qual uma peça preta possa tanto
"mover" quanto "tomar", esta peça é marcada como "tomar". Cada peça só pode ser marcada com uma ação.

Para este exercício, considere que as peças pretas nunca estarão na última linha do jogo (dama). Caso não 
ocorra peça para algum dos movimentos possíveis exibir o valor 0 (zero) ao invés da posição.
*}

(* Implementacao Ingenua *)
(* Parte mais trabalhosa eh imprimir... *)

Program L10E0;
Uses MatrizUtilitarios In '../../Pascal/Units/MatrizUtilitarios.pas';
Const Linha = 8; Coluna = 8;
      Ficar = 0; Mover = 1; Tomar = 2;

Var I, J, Aux:LongInt;
    M:Matriz;

    Acao, Posi: Array [1..12] Of LongInt;

(* 0 -> Ficar; 1 -> Mover; 2 -> Tomar *)
Function Analisa(Var M:Matriz; I, J:LongInt):LongInt;
Begin
    Analisa:= Ficar;

    If J < Coluna Then Begin (* Pulo Pela Direita*)
        If (J < 7) And (M[I+1][J+1] = 1) And (M[I+2][J+2] = 0) Then Analisa:= Tomar
        Else If M[I+1][J+1] = 0 Then Analisa:= Mover;
    End;

    If J > 1 Then Begin (* Pulo Pela Esquerda *)
        If (J > 2) And (M[I+1][J-1] = 1) And (M[I+2][J-2] = 0) Then Analisa:= Tomar 
        Else If (M[I+1][J-1] = 0) And (Analisa < Tomar) Then Analisa:= Mover;
    End;

End;

Begin
    For Aux:= 1 To 12 Do 
    Begin
        Acao[Aux]:= -1;
        Posi[Aux]:= 0;
    End;
    Aux:= 1;
   
    LeMatriz(M, Linha, Coluna); 

    For I:= 1 To Linha Do
    Begin
        For J:= 1 To Coluna Do
        Begin
            If M[I][J] = -1 Then Begin  (* Preta *)
                Acao[Aux]:= Analisa(M, I, J);
                Posi[Aux]:= I*10 + J; 
                Aux += 1;
            End;
        End;
    End;

    (* Imprimindo ... *)
    For I:= Tomar Downto Ficar Do 
    Begin
        J:= 0;
        Case I Of 
            Tomar: Write('tomar: ');
            Mover: Write('mover: ');
            Ficar: Write('ficar: ');
        End;

        For Aux:= 1 To 12 Do
            If Acao[Aux] = I Then  Begin
                Write(Posi[Aux] Div 10,'-', Posi[Aux] Mod 10, ' ');
                J:= 1;
            End;

        If J = 0 Then Write(0);
        Writeln();
    End;

End.

