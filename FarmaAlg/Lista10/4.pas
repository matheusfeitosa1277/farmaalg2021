{*
Seguindo o racíocinio do exercício sobre quadrado mágico. Crie um programa em Free Pascal que leia um 
inteiro n que representa o tamanho do lado de uma matriz A e uma matriz A(n x n) que representa o quadrado e 
imprima quantas matrizes não triviais (isto é, não pode ser a matriz que é constituida por apenas um elemento, 
uma linha e uma coluna) formam um quadrado mágico a partir da matriz fornecida.
*}

(* Implementacao Super Ingenua - BackTraking, Vetores Auxiliares e outra tecnicas podem ser usadas *)
(* Ler mais sobre a constante magica *)
Program L10E4;
Uses Math,
     MatrizUtilitarios In '../../Pascal/Units/MatrizUtilitarios.pas';
Var N, NTrivial, I, J, Aux:LongInt;
    M:Matriz;

Function QuadradoMagico2(Var M:Matriz; I, J, N:LongInt):LongInt;
Var ConstMagica, K, L, Linha, Coluna:LongInt;
Begin
    QuadradoMagico2:= 0; ConstMagica:= 0;

    (* Constante Magica *)
    For L:= J To J + N Do ConstMagica += M[I][L];

    (* Checando Linhas e Colunas *)
    K:= I; L:= J;
    For K:= I To I + N Do 
    Begin
        Linha:= 0; Coluna:= 0;
        For L:= J To J + N Do 
        Begin
            Linha += M[K][L];
            Coluna += M[L][K];
        End;

        If (Linha <> ConstMagica) Or (Coluna <> ConstMagica) Then Exit;
    End;

    (* Checando Diagonais *)
    Linha:= 0; Coluna:= 0; L:= J + N;
    For K:= I To I + N Do
    Begin
        Linha += M[K][K];
        Coluna += M[K][L];
        L -= 1;
    End;

    If (Linha <> ConstMagica) Or (Coluna <> ConstMagica) Then Exit;

    QuadradoMagico2:= 1;
End;
Begin
    Read(N);
    LeMatriz(M, N, N);

    NTrivial:= 0;
    For I:= N - 1 Downto 1 Do
    Begin
        For J:= N - 1 Downto 1 Do 
            For Aux:= N - Max(I, J) Downto 1 Do
                NTrivial += QuadradoMagico2(M, I, J, Aux);
    End;

    Writeln(NTrivial);
End.

