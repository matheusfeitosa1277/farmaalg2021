{*
Faça um programa Pascal que receba uma sequência de números inteiros terminada em 0 e calcule a média 
aritmética dos valores lidos. Imprima esta média com precisão de duas casas decimais. O valor zero não deve 
ser processado, ele serve para marcar o final da entrada de dados.
*}

Program L2E22;
Var I, Valor, Soma:LongInt;

Begin
    I:= -1; Soma:= 0;
    Repeat
        I:= I + 1;
        Read(Valor);
        Soma:= Soma + Valor;
    Until Valor = 0;

    If I <> 0 Then Writeln(Soma/I:0:2);
End.

