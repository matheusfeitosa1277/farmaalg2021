{*
Sabendo-se que a água se solidifica a zero grau Celsius, ou a 32 Fahrenheit, e que entra em ebulição a 100 graus 
Celsius ou 212 Fahrenheit, faça um programa Pascal que obtenha do teclado um inteiro que é o valor de 
temperatura em Fahrenheit e imprima na tela o valor correspondente em Celsius e uma mensagem indicando 
se a água nesta temperatura está no estado liquido, sólido ou gasoso. A fórmula de conversão entre graus
Celsius e Farenheit é:

    5F - 9C - 160 = 0

*}

Program L2E0;
Var Fahrenheit:LongInt; Celsius:Real;

Begin
  Read(Fahrenheit);
  Celsius:= (5*Fahrenheit - 160)/9;
  Writeln(Celsius:0:2);

  If (Celsius <= 0) 
    Then Writeln('solido')
    Else If (Celsius >= 100) 
      Then  Writeln('gasoso')
      Else Writeln('liquido');
End.

