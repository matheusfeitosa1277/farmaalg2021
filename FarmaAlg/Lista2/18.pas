{*
Faça um programa Pascal que receba um número inteiro positivo N e calcule a produto dos N primeiros
números pares positivos. Ao final, imprima este produto. Para esse exercício, considere o primeiro número par
como sendo 2.
*}

Program L2E18;
Var N, Soma:LongInt;

Begin
    Read(N);
    Soma:= 1;

    While N <> 0  Do 
    Begin
        Soma:= Soma * 2*N;
        N:= N-1;
    End;
    Writeln(Soma);

End.

