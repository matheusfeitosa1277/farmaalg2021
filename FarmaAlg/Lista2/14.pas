{*
Alguém deseja cobrir as paredes de uma cozinha com azulejos. As lojas somente vendem caixas com 10
azulejos. Todas as lojas do ramo vendem apenas 3 (três) tipos de azulejos, cujas dimensões são:

    50cm x 40cm;
    50cm x 60cm;
    50cm x 80cm.

Faça um programa Pascal que leia do teclado dois valores inteiros representando respectivamente o tipo do
azulejo desejado (um dos números 1, 2 ou 3) e a área que se deseja azulejar, em metros quadrados. Seu
programa deve imprimir a quantidade de caixas de azulejos que deverão ser compradas para cobrir toda a área.
Considere que pedaços de azulejo podem ser reaproveitados, de maneira a minimizar a quantidade de caixas.
*}

Program L2E14;
Uses math;
Var Tipo, Area:LongInt;


Begin
    Read(Tipo, Area);

    Case Tipo Of
        1 : Write(ceil(Area/2));
        2 : Write(ceil(Area/3));
        3 : Write(ceil(Area/4));
    End;
    Writeln(' caixas');

End.

