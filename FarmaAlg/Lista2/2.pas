{*
Faça um programa Pascal que leia do teclado um valor inteiro representando o ano de nascimento de uma 
pessoa.
Com esse dado, o programa deve fazer o seguinte: 
    1.Calcular e imprimir sua idade, considerando que estamos no ano de 2020; 
    2.Verificar e imprimir se a pessoa já tem idade para votar (16 anos ou mais);
    3.Verificar e imprimir se a pessoa já tem idade para conseguir a carteira de habilitação (18 ou mais).
*}

Program L2E2;
Var Ano, Idade:LongInt;

Begin
  Read(Ano);
  Idade:= 2020 - Ano;
  Writeln(Idade);

  If (Idade >= 16) 
    Then Writeln('SIM')
    Else Writeln('NAO');
  If (Idade >= 18)
    Then Writeln('SIM')
    Else Writeln('NAO');
End.

