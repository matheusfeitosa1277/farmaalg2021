{*
Faça um programa Pascal que leia do teclado um número inteiro positivo. Esse número deve ser classificado em 
uma das seguintes situações:
    .Múltiplo exclusivamente de 7.
    .Múltiplo exclusivamente de 11.
    .Múltiplo de 7 e de 11.
    .Não é múltiplo nem de 7 nem de 11.
O programa deve imprimir a situação correspondente ao número lido. 
*}

Program L2E3;
Var Multiplo:LongInt;

Begin
  Read(Multiplo);
  If (Multiplo Mod 77 = 0) 
    Then Writeln('Multiplo de 7 e de 11.')
    Else If (Multiplo Mod 11 = 0)
      Then Writeln('Multiplo exclusivamente de 11.')
      Else If (Multiplo Mod 7 = 0)
        Then Writeln('Multiplo exclusivamente de 7.')
        Else Writeln('Nao e multiplo nem de 7 nem de 11.');
End.

