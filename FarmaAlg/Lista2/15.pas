{*
Faça um programa Pascal que leia do teclado um conjunto de 4 valores i, a, b, c, sendo que i é um valor inteiro e
positivo e a, b, c, são quaisquer valores reais. Imprima na tela os valores de a, b, c da seguinte forma:

    . os três valores a, b, c em ordem crescente, se i = 1
    . os três valores a, b, c em ordem decrescente, se i = 2,
    . os três valores a, b, c de forma que o maior dentre a, b, c fique entre os outros dois valores, com a ordem 
      deles mantida, se i = 3

*}

Program L2E15;
Var I:LongInt;
    A, B, C:LongInt;
    Maior, Menor, Mediana:LongInt;

Begin
    Read(I, A, B, C);

    //Fugindo um pouco de Alg 1 ... 
    //Vamos ver as arvores de decisao destes problemas para
    //Chegarmos em respostas otimas
   
    (* Ordenacao Crescente de 3 Valores *)
    //
    //                        1                        (A < B)                        0                        
    //          1          (B < C)          0                                      (B < C]                     
    //       [A B C]                 1   (A < C)   0             1   (A < C)   0                 [C B A]       
    //                            [A C B]       [C A B]       [B A C]       [B C A]                            
    
    //  If A < B Then                                             
    //      If B < C Then Writeln(A, ' ', B, ' ', C)                              
    //      Else                                                                  
    //          If A < C Then Writeln(A, ' ', C, ' ', B)                          
    //          Else Writeln(C,' ', A, ' ', B)                                    
    //  Else                                                                      
    //      If B < C Then                     
    //          If A < C Then Writeln(B, ' ', A, ' ', C)
    //          Else Writeln(B, ' ', C, ' ', A)         
    //      Else Writeln(C, ' ', B, ' ', A);            
    //

    (* Encontado Maior, Menor e Mediana dos valores *)
    If A < B Then                                             
        If B < C Then Begin Menor:= A; Mediana:= B; Maior:= C; End                             
        Else Begin                                                                 
            Maior:= B;
            If A < C Then Begin Menor:= A; Mediana:= C; End                          
            Else Begin Menor:= C; Mediana:= A; End                                   
        End
    Else                                                                      
        If B < C Then Begin                    
            Menor:= B;
            If A < C Then Begin Mediana:= A; Maior:= c; End
            Else  Begin Mediana:= C; Maior:= A; End      
        End 
        Else Begin Menor:= C; Mediana:= B; Maior:= A; End;            

    Case I Of
        1:  Writeln(Menor, ' ', Mediana, ' ', Maior); (* Ordenacao Crescente *)
        2:  Writeln(Maior, ' ', Mediana, ' ', Menor); (* Ordenacao Decrescente *)
        3:  If (A = Maior) 
                Then Writeln(B, ' ', A, ' ', C)
                Else If C = (Maior)
                    Then Writeln(A, ' ', C, ' ', B)
                    Else Writeln(A, ' ', B, ' ', C);
    End;
End.

