{*
Faça um programa Pascal que leia do teclado três números inteiros representando as medidas dos lados de
um triângulo e imprima se ele é equilátero (todos os lados iguais), isósceles (dois lados iguais) ou escaleno
(todos os lados diferentes). Não é necessário verificar se os lados formam um triângulo. 
*}

Program L2E7;
Var L1, L2, L3:LongInt;

Begin
    Read(L1, L2, L3);
    If (L1 = L3) And (L1 = L2) 
        Then Writeln('Equilatero')
        Else If (L3 = L2)
            Then Writeln('Isosceles')
            Else Writeln('Escaleno');
End.

