{*
Faça um programa Pascal que receba um número inteiro positivo N e calcule a soma dos N primeiros números 
ímpares positivos. Ao final, imprima esta soma.
*}

Program L2E19;
Var N, Soma:LongInt;

Begin
    Read(N);
    Soma:= 1;
    While N <> 1 Do
    Begin
        Soma:= Soma + 2*N - 1;
        N:= N-1;
    End;

    Writeln(Soma);
        
End.

