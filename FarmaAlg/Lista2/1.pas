{*
Faça um programa Pascal que leia do teclado um valor inteiro que é a área de um cômodo e imprima a potência 
de iluminação necessária para iluminá-lo de acordo com a seguinte relação: 

    100 watts para comodos com 6m^2 ou menos

    80 watts para os primeiros 3m^2 e mais 15 watts a cada m^2 de acrescimo para comodos maiores que 6m^2

*}

Program L2E1;
Var Area:LongInt;

Begin
  Read(Area);
  If (Area <= 6) 
    Then Writeln(100)
    Else Writeln(80 + 15*(Area - 3));
End.

