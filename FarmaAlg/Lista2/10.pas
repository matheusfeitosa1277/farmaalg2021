{*
Faça um programa Pascal que leia do teclado dois valores inteiros que são as coordenadas (X,Y) de um ponto no
sistema cartesiano. Imprima na tela o quadrante ao qual o ponto pertence: 1, 2, 3 ou 4, conforme as regras
clássicas da matemática.

Caso o ponto não pertenca a nenhum quandrante, imprima X se ele está sobre o eixo X, Y, se ele está sobre o
eixo Y, ou então imprima O, caso ele esteja na origem. 
*}

Program L2E10;
Var X, Y:LongInt;

Begin
    Read(X, Y);

    If (X <> 0) And (Y <> 0) Then    
    Begin
        If (X > 0) Then
        Begin
            If  Y > 0
                Then Writeln(1)
                Else Writeln(4);
        End
        Else
            If Y > 0
                Then Writeln(2)
                Else Writeln(3);
    End
    Else If (X = Y) 
        Then Writeln('O')
        Else If (X = 0)
            Then Writeln('Y')
            Else Writeln('X');
End.

