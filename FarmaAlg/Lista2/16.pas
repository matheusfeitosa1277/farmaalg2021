{*
Faça um programa Pascal que leia do teclado um valor real que representa o salário mensal de uma pessoa. Seu
programa deve imprimir valor do imposto de renda (IR) mensal, em reais, de acordo com a tabela de 2009, que
está abaixo.

Se o salário digitado for menor que o salário mínimo de R$ 540,00 o programa deve imprimir "NAO". 


    1. menor ou igual a 1424,00: 0%;
    2. maior que 1424,00, menor ou igual a 2150,00: 7.5%;
    3. maior que 2150,00, menor ou igual a 2866,00: 15%;
    4. maior que 2866,00, menor ou igual a 3582,00: 22.5%;
    5. maior que 3582,00: 27.5%.

Junto com o valor do IR mensal, o programa deve imprimir a Faixa (1,2,3,4 ou 5) correspondente ao salário.

*}

Program L2E16;
Var Salario:Real;

Begin
    Read(Salario);

    If Salario < 540 
        Then Writeln('NAO')
        Else 
            If      Salario <= 1424 Then Writeln(1, ' ', Salario*0:0:2) 
            Else If Salario <= 2150 Then Writeln(2, ' ', Salario*0.075:0:2)
            Else If Salario <= 2866 Then Writeln(3, ' ', Salario*0.150:0:2)
            Else If Salario <= 3582 Then Writeln(4, ' ', Salario*0.225:0:2)
            Else                         Writeln(5, ' ', Salario*0.275:0:2);
End.

