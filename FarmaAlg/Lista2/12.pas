{*
Uma empresa concederá um aumento de salário aos seus funcionários, mas este aumento será de acordo com
o cargo que cada um ocupa. A tabela abaixo contém os códigos, o cargo e e percentual de aumento
correspondente. 

Faça um programa Pascal que leia dois valores do teclado, o primeiro é o salário de um funcionário e o segundo
é o código do cargo dele. Calcule o valor do novo salário.
Se o cargo do funcionário não estiver na tabela, ele deverá receber 40% de aumento. Imprima o valor do salário
antigo, o do novo salário e a diferença entre eles, nesta ordem, em 3 linhas.

    Código  Cargo   Percentual
    101     Gerente     10%
    102     Engenheiro  20%
    103     Técnico     30%

*}

Program L2E12;
Var Salario, Aumento:Real;
    Cargo:LongInt;

Begin
    Read(Salario, Cargo);

    Case Cargo Of
        101 : Aumento:= Salario * 0.1;
        102 : Aumento:= Salario * 0.2;
        103 : Aumento:= Salario * 0.3;
    Else
        Aumento:= Salario * 0.4;
    End;

    Writeln(Salario:0:2);
    Writeln(Salario + Aumento :0:2);
    Writeln(Aumento:0:2);
End.

