{*
Faça um programa Pascal que dada a idade de um nadador classifica-o em uma das seguintes categorias:

    1. 5 a 7 anos;
    2. 8 a 10 anos;
    3. 11 a 13 anos;
    4. 14 a 17 anos;
    5. maiores de 18 anos.

Seu programa deve imprimir a categoria do nadador correspondente ao valor da idade lida do teclado, que 
deve ser um valor inteiro.
*}

Program L2E17;
Var Idade:LongInt;

Begin
    Read(Idade);

    Case Idade Of
           0  : ;
        5..7  : Writeln(1);
        8..10 : Writeln(2);
       11..13 : Writeln(3);
       14..17 : Writeln(4);
    Else
        Writeln(5);
    End;
End.

