{*
Faça um programa Pascal que leia do teclado três valores reais que são as três notas obtidas por uma pessoa. 
Leia também do teclado a quantidade de faltas dessa pessoa.
    
    Caso o resultado da média aritmética seja inferior a 4.0 o programa deverá imprimir "NAO". 
    Caso a média seja maior ou igual e 4.0 e inferior a 7.0, imprima "TALVEZ".
    Caso a média seja maior ou igual a 7.0 imprima "SIM".

Alunos com o número de faltas maior ou igual a 10 estarão automaticamente reprovados. Neste último caso, 
imprima "NAO".
*}

Program L2E5;
Var Nota1, Nota2, Nota3, Faltas, Media:Real;

Begin
  Read(Nota1, Nota2, Nota3, Faltas);
  Media:= (Nota1 + Nota2 + Nota3)/3;

  If (Faltas >= 10)
    Then Writeln('NAO')
    Else If (Media < 4)
      Then Writeln('NAO')
      Else If (Media >= 7)
        Then Writeln('SIM')
        Else Writeln('TALVEZ');
End.

