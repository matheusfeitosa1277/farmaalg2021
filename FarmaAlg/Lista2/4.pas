{*
Faça um programa Pascal que leia do teclado três valores inteiros representando a data de nascimento de uma 
pessoa (dia, mês e ano) e imprima quantos anos completos ela terá no dia 29/04/2021.
*}

Program L2E4;
Var Dia, Mes, Ano:LongInt;

Begin
  Read(Dia, Mes, Ano);
  If (Mes = 4) And (Dia > 29) Then 
    Ano:= Ano + 1;
  If (Mes > 4) Then
    Ano:= Ano + 1;

  Writeln(2021 - Ano);
End.

