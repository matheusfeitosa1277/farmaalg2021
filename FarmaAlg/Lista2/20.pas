{*
Faça um programa Pascal que receba um número positivo N e imprima na tela a soma dos N primeiros números
da sequência de Fibonacci. Os dois primeiros números da sequência são 0 e 1, e os próximos são dados pela 
soma dos dois últimos números anteriormente calculados. 

A título de exemplo, os oito primeiros valores dessa sequência são: 0, 1, 1, 2, 3, 5, 8, 13. 
*}

Program L2E20;
Var N, F1, F2, Aux, Soma:LongInt;

Begin
    Read(N);

    F1:= 0; F2:= 1;
    Soma:= 0;
    While N <> 1 Do
    Begin
        Soma:= Soma + F2;

        Aux:= F2;
        F2:= F2 + F1;
        F1:= Aux;

        N:= N-1;
    End;

    Writeln(Soma);

End.

