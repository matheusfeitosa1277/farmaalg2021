{*
Um vendedor necessita de um programa que calcule o preço total devido por um cliente que comprou um 
produto em sua loja. Faça um programa Pascal que receba dois números inteiros que são, respectivamente, o 
código do produto e a quantidade comprada. Imprima na tela o preço total, usando a tabela abaixo.
Caso o código não exista o programa deve imprimir ERRO.

    Código do Produto   Preço unitário
    1001    5,32
    1324    6,45
    6548    2,37
    987     5,32
    7623    6,45

*}

Program L2E9;
Var Codigo, Quantidade:LongInt;

Begin
  Read(Codigo, Quantidade);

  Case Codigo Of
    1001 : Writeln(Quantidade * 5.32:0:2);
    1324 : Writeln(Quantidade * 6.45:0:2);
    6548 : Writeln(Quantidade * 2.37:0:2);
     987 : Writeln(Quantidade * 5.32:0:2);
    7623 : Writeln(Quantidade * 6.45:0:2);
  Else
    Writeln('ERRO');
  End;
End.

