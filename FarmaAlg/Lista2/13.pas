{*
Faca  um programa Pascal que leia dois números inteiros representando respectivamente o número de lados de
um polígono regular e a medida do lado. Seu programa deve fazer o seguinte: 

    se o número de lados for 3, imprima TRIANGULO e o valor do seu perímetro;
    se o número de lados for 4, imprima QUADRADO e o valor da sua área;
    se o número de lados for 5, imprima PENTAGONO;
    se o número de lados for menor que 3 imprima a mensagem "ERRO";
    se o número de lados for maior que 5 imprima a mensagem "ERRO".

*}

Program L2E13;
Var Lados, Tamanho:LongInt;

Begin
    Read(Lados, Tamanho);

    Case Lados Of
        3 : Begin Write('TRIANGULO '); Write(Lados*Tamanho); End;
        4 : Begin Write('QUADRADO '); Write(Tamanho*Tamanho); End;
        5 : Write('PENTAGONO');
    Else
        Writeln('ERRO');
    End;

End.

