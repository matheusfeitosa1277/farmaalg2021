{*
Faça um programa Pascal que leia uma sequência de números inteiros terminada em zero e imprima o maior e 
o menor número dessa sequência. O valor zero não deve ser ser processado, ele serve para marcar o final da 
entrada de dados.
*}

Program L2E21;
Var N, Max, Min:LongInt;

Begin

    Max:= -MAXINT; MIN:= MAXINT;  
    Repeat
        Read(N);
        IF N <> 0 Then Begin
            If N > Max Then Max:= N;
            If N < Min Then Min:= N;
        End;
    Until N = 0;

    Writeln(Max, ' ', Min);

End.

