{*
Faca um programa Pascal que leia do teclado três valores inteiros representando respectivamente a base
inferior, base superior e altura de um
trapezio qualquer em centímetros. O programa deve calcular a área do trapézio usando a fórmula:

    A = [(B+b)/2]*h

Onde B é a base inferior, b é a base superior e h é a altura. Ao final, o valor da área deve ser impresso na tela.
Se as três medidas formarem um quadrado, deve também ser impresso na tela uma mensagem escrita "SIM", caso
contrário, imprima "NAO".
*}

Program L2E6;
Var B, Bm, H:LongInt;
    A:Real;

Begin
    Read(B, Bm, H);
    A:= (B + Bm)/2 * h;
    Writeln(A:0:2);

    If (H = B) And (H = Bm) 
        Then Writeln('SIM')
        Else Writeln('NAO');
End.

