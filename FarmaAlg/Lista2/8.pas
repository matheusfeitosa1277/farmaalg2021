{*
O Índice de Massa Corporal (IMC) é uma medida usada para se calcular obesidade. O IMC de um adulto com
peso considerado saudável tem valores entre 18,5 e 25. Considere a fórmula:

    IMC = peso/(altura^2)

Considere também que o peso é medido em quilogramas e a altura em metros. 

Faça um programa Pascal que leia do teclado um inteiro representando o peso e um real representando a
altura de uma pessoa (nesta ordem) e imprima na tela o IMC dela calculado com o peso e a altura fornecidos.
Se ela estiver no peso saudável, imprima SIM, caso contrário, imprima NAO. Caso a altura seja 0, imprima
ERRO.

*}

Program L2E8;
Var Alt, Imc:Real;
    Peso:LongInt;

Begin
    Read(Peso, Alt);

    If Alt <> 0 Then
    Begin
        Imc:= Peso/(Alt*Alt);
        
        Writeln(Imc:0:2);
        If (Imc >= 18.5) And (Imc <= 25) 
            Then Writeln('SIM')
            Else Writeln('NAO');
    End
    Else Writeln('ERRO');
       

End.

