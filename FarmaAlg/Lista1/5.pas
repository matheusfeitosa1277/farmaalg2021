{*
Faça um programa Pascal que leia um número inteiro e imprima o seu sucessor e seu antecessor, na mesma linha.
*}

Program L1E5;
Var N:LongInt;

Begin
  Read(N);
  Writeln(N+1, ' ', N-1);
End.

