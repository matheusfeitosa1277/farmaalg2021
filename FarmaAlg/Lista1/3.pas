{*
Faça um programa Pascal que leia dois números inteiros e imprima a média aritmética entre eles.
*}

Program L1E3;
Var N1, N2:LongInt;

Begin
  Read(N1, N2);
  Writeln((N1+N2)/2:0:0);
End.

