{*
Considere que o número de uma placa de veículo é composto por quatro algarismos. Faça um programa Pascal 
que leia um número inteiro do teclado e apresente o algarismo correspondente à casa do milhar. Use o 
operador DIV.
*}

Program L1E33;
Var N:LongInt;

Begin
  Read(N);
  Writeln(N Div 1000);
End.

