{*
Dado um número inteiro que representa uma quantidade de segundos, faça um programa que imprima o seu
valor equivalente em horas, minutos e segundos, nesta ordem. Se a quantidade de segundos for insuficiente
para dar um valor em horas, o valor em horas deve ser 0 (zero). A mesma observação vale em relação aos
minutos e segundos. 
*}

Program L1E10;
Var Segundos, Vezes, I, Aux:LongInt;

Begin
    Read(Segundos);
    Aux:= 60*60;

    For I:= 2 Downto 1 Do
    Begin
        Vezes:= Segundos div Aux;
        Write(Vezes,':');
        If Segundos >= Aux Then Segundos:= Segundos - Vezes*Aux;
        Aux:= Aux div 60;
    End;
    Write(Segundos);

End.






