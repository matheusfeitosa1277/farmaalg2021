{*
Faça um programa Pascal que, usando apenas atribuições e expressões aritméticas, imprima ao contrário um 
número inteiro de três digitos lido pelo teclado. Desconsidere números que começam ou terminam em zero.
*}

Program L1E36;
Var N:LongInt;

Begin
    Read(N);
    Write(N Mod 10);
    Write(N mod 100 Div 10);
    Write(N div 100);
End.

