{*
Faça um programa Pascal que leia dois números inteiros do teclado e efetue a adição. Caso o valor somado seja 
maior que 20, este deverá ser apresentado somando-se a ele mais 8; caso o valor somado seja menor ou igual a 
20, este deverá ser apresentado subtraindo-se 5.
*}

Program L1E25;
Var N1, N2, S:LongInt;

Begin
  Read(N1, N2);
  S:= N1 + N2;

  If S > 20
    Then Writeln(S + 8)
    Else Writeln(S - 5);
End.

