{*
Faça um programa Pascal que leia do teclado dois valores reais x e y, e em seguida
calcule e imprima o valor da seguinte expressão com três casas decimais:
x/y + y/x
*}

Program L1E2;
Var X, Y: Real;

Begin
  Read(X, Y);
  Writeln((X/Y + Y/X):0:3);
End.

