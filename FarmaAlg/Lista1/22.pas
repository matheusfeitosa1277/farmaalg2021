{*
Faça um programa Pascal que leia do teclado um número inteiro e imprima SIM caso o número seja impar,
negativo e menor que -20 ou então se for par, positivo e maior que 7.
Caso contrário imprima NAO. 
A dica é usar uma combinação correta que envolva os operadores AND e OR.
*}

Program L1E22;
Var N:LongInt;

Begin
    Read(N);

    If (N Mod 2 = 0)
        Then If N > 7 
            Then Writeln('SIM')
            Else Writeln('NAO')
        Else If N < -20 
            Then Writeln('SIM')
            Else Writeln('NAO');
End.

