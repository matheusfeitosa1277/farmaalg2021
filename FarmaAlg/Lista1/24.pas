{*
Faça um programa Pascal que leia do teclado dois valores inteiros e os some. Se o resultado for maior que 10, 
imprima o primeiro valor, caso contrário, imprima o segundo.
*}

Program L1E24;
Var N1, N2, S:LongInt;

Begin
  Read(N1, N2);
  S:= N1 + N2;
  If (S > 10) 
    Then Writeln(N1)
    Else Writeln(N2);
End.

