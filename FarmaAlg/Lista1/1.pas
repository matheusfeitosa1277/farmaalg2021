{*
Faça um programa Pascal que leia um número real i
representando o diâmetro (em metros) de uma esfera.
Calcule e imprima o volume desta esfera, com duas casas de precisão.
Lembre-se que o volume de uma esfera é dado pela fórmula 
V = 4pi/3 * R^3
Use pi=3.14
*}

Program L1E1;
Const P = 3.14;
Var Diametro, Volume, Raio: Real;

Begin
  Read(Diametro);
  Raio:= Diametro/2; 
  Volume:= (4*P)/3 * Raio * Raio * Raio;
  Writeln(Volume:0:2);
End.

