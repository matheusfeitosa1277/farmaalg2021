{*
Uma P.A. (Progressão Aritmética) é determinada pela sua razão r e pelo primeiro termo (a1). Faça um programa 
Pascal que seja
capaz de imprimir o enésimo (n) termo (an) de uma P.A., dado a razão (r) e o primeiro termo (a1). Seu programa 
deve ler três valores inteiro
do teclado (n, r, a1) do teclado e imprimir an, segundo a fórmula:

    an = a1 + (n - 1)*r

*}

Program L1E27;
Var N, R, A1:LongInt;

Begin
  Read(N, R, A1);
  Writeln(A1 + (N - 1)*R);
End.

