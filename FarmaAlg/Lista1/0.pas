{*
Faça um programa Pascal que leia um número real do teclado e 
imprima a terça parte deste número com duas casas decimais.
*}

Program L1E0;
Var Numero, TercaParte: Real;

Begin
  Read(Numero);
  TercaParte:= Numero/3;
  Writeln(TercaParte:0:2);
End.

