{*
onsidere que o número de uma placa de veículo é composto por quatro algarismos. Faça um programa Pascal
que leia um número inteiro do teclado e imprima o algarismo correspondente à casa das centenas. Use os
operadores DIV e MOD.
*}

Program L1E34;
Var N:LongInt;

Begin
    Read(N);
    N:= (N Mod 1000) Div 100;
    Writeln(N);
End.

