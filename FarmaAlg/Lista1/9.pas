{*
Faça um programa Pascal que leia três números inteiros (P1, P2 e P3) contendo as notas das três provas de
um aluno em uma certa disciplina e imprima a média final deste aluno, a qual deve ser um número inteiro.
Considerar que a média é ponderada e que o peso das notas é 1, 2 e 3, respectivamente. A fórmula que
calcula essa média é:
(p1 + 2*p2 + 3*p3)/(1 + 2 + 3)
*}

Program L1E9;
Var P1, P2, P3:LongInt;

Begin
  Read(P1, P2, P3);
  Writeln(trunc((P1 + 2*P2 + 3*P3)/6));
End.

