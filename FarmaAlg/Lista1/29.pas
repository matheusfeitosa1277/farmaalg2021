{*
Considere a razão r de uma P.A. (Progressão Aritmética) e um termo qualquer, k (ak). Escreva um programa 
Pascal para calcular o enésimo termo n (an). Seu programa deve ler k, ak, r, n do teclado e imprimir an, segundo 
a fórmula:

    an = ak + (n - k)*r

*}

Program L1E29;
Var K, Ak, R, N:LongInt;

Begin
  Read(K, Ak, R, N);
  Writeln(Ak + (N - K)*R);
End.

