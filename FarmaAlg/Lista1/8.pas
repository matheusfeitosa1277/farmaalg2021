{*
Sabe-se que para iluminar de maneira correta os cômodos de uma casa, para cada Metro quadrado (m2)
deve-se usar 18W de potência.
Faça um programa Pascal que:

   1.receba dois inteiros representando as duas dimensões de um comodo em metros;
   2.calcule e imprima a sua área em m2;
   3.imprima a potência de iluminação que deverá ser usada em watts.
*}

Program L1E8;
Var D1, D2, Area:LongInt;

Begin
  Read(D1, D2);
  Area:= D1*D2;
  Writeln(Area, ' ', Area*18);
End.

