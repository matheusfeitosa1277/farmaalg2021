{*
Faça um programa Pascal que leia do teclado um número inteiro N e imprima se ele é PAR ou IMPAR.
*}

Program L1E15;
Var N:LongInt;

Begin
  Read(N);
  If (N Mod 2) = 0
    Then Writeln('PAR')
    Else Writeln('IMPAR');
End.

