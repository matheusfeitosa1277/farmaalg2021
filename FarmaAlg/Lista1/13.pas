{*
Faça um programa Pascal que leia do teclado dois números inteiros e imprima qual é o menor valor entre eles.
*}

Program L1E13;
Var N1, N2:LongInt;

Begin
  Read(N1, N2);
  If (N1 < N2)
    Then Writeln(N1)
    Else Writeln(N2);
End.

