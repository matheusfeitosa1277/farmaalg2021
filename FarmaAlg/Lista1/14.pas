{*
Faça um programa Pascal que leia um número inteiro do teclado, calcule se ele é ou não divisível por 5.
Imprima SIM caso ele seja e NAO em caso contrário.
*}

Program L1E14;
Var N:LongInt;

Begin
  Read(N);
  If ((N mod 5) = 0)
    Then Writeln('SIM')
    Else Writeln('NAO');
End.

