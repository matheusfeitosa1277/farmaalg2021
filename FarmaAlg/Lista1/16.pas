{*
Faça um programa Pascal que leia de teclado um número inteiro e imprima se este é múltiplo de 3.
*}

Program L1E16;
Var N:LongInt;

Begin
  Read(N);
  If (N Mod 3) = 0
    Then Writeln('SIM')
    Else Writeln('NAO');
End.

