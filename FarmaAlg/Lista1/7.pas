{*
O custo ao consumidor de um carro novo é a soma do custo de fábrica com a percentagem do distribuidor
e dos impostos (aplicados ao custo de fábrica).
Supondo que a percentagem do distribuidor seja de 28% e os impostos de 45%, faça um programa Pascal que
leia um número inteiro representando o custo de fábrica de um carro e imprima o custo ao consumidor.

PS: Considere sempre o arredondamento com Truncamento.
*}


Program L1E7;
Var Custo:LongInt;

Begin
  Read(Custo);
  Writeln(trunc(Custo + Custo*0.28 + Custo*0.45));
End.

