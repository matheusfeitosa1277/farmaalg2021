{*
Faça um programa Pascal que leia um número inteiro do teclado e imprima o cubo do número caso ele seja 
positivo ou igual a zero e o quadrado do número caso ele seja negativo.
*}

Program L1E19;
Var N, Q:LongInt;

Begin
  Read(N);
  Q:= N*N;
  If N >= 0
    Then Writeln(Q*N)
    Else Writeln(Q);
End.

