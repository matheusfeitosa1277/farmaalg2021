{*
Faça um programa Pascal que leia do teclado dois valores inteiros x e y, e em seguida calcule
e imprima o valor da seguinte expressão: 
x^3 + xy
*}

Program L1E4;
Var X, Y:LongInt;

Begin
  Read(X, Y);
  Writeln(X*X*X + X*Y);
End.

