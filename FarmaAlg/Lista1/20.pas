{*
Faça um programa Pascal que leia um número, calcule se ele é divisível por 3 e por 7. Caso seja, imprima SIM e 
caso não seja imprima NAO. Dica: use o operador AND.
*}

Program L1E20;
Var N:LongInt;

Begin
  Read(N);
  If ((N Mod 3) = 0) And ((N Mod 7) = 0)
    Then Writeln('SIM')
    Else Writeln('NAO');
End.

