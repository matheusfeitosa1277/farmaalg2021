{*
Faça um programa Pascal que leia dois números inteiros, um será o valor de um produto e outro o valor de
desconto que esse produto está recebendo.
Imprima quantos reais o produto custa na promoção.
*}

Program L1E6;
Var Valor, Desconto:LongInt;

Begin
  Read(Valor, Desconto);
  Writeln(Valor - Desconto);
End.

