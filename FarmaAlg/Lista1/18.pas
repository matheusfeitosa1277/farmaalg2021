{*
Faça um programa Pascal que leia dois números inteiros do teclado e imprima SIM se o primeiro número é 
divisível pelo segundo.
*}

Program L1E18;
Var N1, N2: LongInt;

Begin
  Read(N1, N2);
  If (N1 Mod N2) = 0
    Then Writeln('SIM')
    Else Writeln('NAO');
End.

