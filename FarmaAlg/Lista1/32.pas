{*
Considere que o número de uma placa de veículo é composto por quatro algarismos. Faça um programa Pascal 
que leia um número inteiro do
teclado e imprima o algarismo correspondente à casa das unidades.
Use o operador MOD.
*}

Program L1E32;
Var N:LongInt;

Begin
    Read(N);
    Writeln(N Mod 10);
End.

