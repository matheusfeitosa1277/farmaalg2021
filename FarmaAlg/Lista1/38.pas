{*
Faça um programa Pascal que leia um número real representando o cosseno do ângulo formado por uma 
escada apoiada no chão e a distância em que a escada está de uma parede. Calcule e imprima com 3 casas 
decimais a altura em que a escada toca a parede.
*}

Program L1E38;
Var Coss, Dist, Hip, Alt:Real;

Begin
    Read(Coss, Dist);
    Hip:= Dist/Coss;
    Alt:= Sqrt(Hip*Hip-Dist*Dist);
    
    Writeln(Alt:0:3);
End.

