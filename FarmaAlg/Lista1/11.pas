{*
Você é um vendedor de carros e só aceita pagamentos à vista. As vezes é necessário ter que dar troco, mas seus 
clientes não gostam de notas miúdas. Para agradá-los você deve fazer um programa Pascal que receba o valor
do troco que deve ser dado ao cliente e retorna o número de notas de R$ 100 necessárias para esse troco.
*}

Program L1E11;
Var Valor:LongInt;

Begin
    Read(Valor);
    Writeln(Valor div 100);
End.

