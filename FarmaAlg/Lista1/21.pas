{*
Faça um programa Pascal que leia um número inteiro do teclado. Se ele estiver entre os valores -15 e 30 (-15 e 
30 não estão inclusos), imprima seu número oposto, senão imprima o próprio número.
*}

Program L1E21;
Var N:LongInt;

Begin
  Read(N);
  If (-15 < N) And (N < 30)
    Then Writeln(-N)
    Else Writeln(N);
End.

