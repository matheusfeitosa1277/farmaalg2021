{*
Faça um programa Pascal que leia três valores inteiros representando as medidas de 3 lados de um triângulo,
calcule a sua área e imprima o resultado com 3 casas decimais. Sabe-se que a área de um triângulo qualquer 
pode ser calculada pela fórmula:

A = sqrt( p*(p-a)*(p-b)*(p-c) )

Sendo p = (a+b+c)/2

*}

Program L1E12;
Var A, B, C:LongInt;
    P, Area:Real

Begin
    Read(A, B, C);
    P:= (A + B + C)/2;
    Area:= Sqrt( P*(P-A)*(P-B)*(P-C) );
    Writeln(Area:0:3);
End.

