{*
Américo é um vendedor de tapetes caros, mas ele vende somente tapetes de alguns tamanhos. O estoque de
Américo consiste em 3 tipos de tapetes: aqueles com comprimento de 1 até 3 metros e largura entre 1 e 4
metros; aqueles com comprimento entre 5 e 10 metros e largura entre 3 e 5 metros; e, por fim, aqueles com
comprimentro entre 14 e 27 metros e largura entre 9 e 12 metros. Faça um programa Pascal que leia do teclado
dois números inteiros, representando respectivamente o comprimento e a largura do tapete desejado, e
imprima se Américo tem ou não o tapete.
*}

(*Nao ta muito bonito...*)

Program L1E26;
Var Comprimento, Largura:LongInt;

Begin
    Read(Comprimento, Largura);
    
    If (Comprimento < 1) Or (Comprimento > 27) Or (Largura < 1) Or (Largura > 12) Then Writeln('NAO')
    Else If (Comprimento <= 3) And (Largura <= 4) Then Writeln('SIM')
    Else If (Comprimento >= 5) And (Comprimento <= 10) And (Largura >= 3) And (Largura <= 5) Then Writeln('SIM')
    Else If (Comprimento >= 14) And (Largura >= 9) Then Writeln('SIM')
    Else Writeln('NAO');
End.

