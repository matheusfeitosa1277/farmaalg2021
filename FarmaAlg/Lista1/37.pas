{*
Observemos o número 3025. Ele possui a seguinte característica:

    30 + 25 = 55

    55^2 = 3025

Faça um programa Pascal que leia um número inteiro do teclado. Considere que o usuário sempre digita 
números com 4 dígitos sem zeros no início ou final. Imprima na tela uma mensagem indicando se o número tem 
a propriedade citada acima. Dica: use o operandor AND.
*}

Program L1E37;
Var N, Aux:LongInt;

Begin
    Read(N);
    Aux:= (N Mod 10000 Div 100) + (N Mod 100) ;
    If (Aux * Aux) = N 
        Then Writeln('SIM')
        Else Writeln('NAO');
End.

