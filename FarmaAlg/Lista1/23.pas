{*
A prefeitura de Piraporinha abriu uma linha de crédito para os funcionários estatutários. O valor máximo da 
prestação não poderá ultrapassar 30% do salário bruto. Faça um programa Pascal que leia do teclado dois 
números inteiros que representam o salário bruto e o valor da prestação e informe se o empréstimo pode ou 
não ser concedido.
*}

Program L1E23;
Var S, P:LongInt;

Begin
  Read(S, P);
  If (P > S*0.3)
    Then Writeln('NAO')
    Else Writeln('SIM');
End.

