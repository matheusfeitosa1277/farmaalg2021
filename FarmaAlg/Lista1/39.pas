{*
Faça um programa Pascal que, dado um número inteiro de três dígitos lido do teclado, construa um número 
 inteiro de quatro dígitos com as seguintes regras: 
    os três primeiros dígitos, contados da esquerda para a direita, são iguais aos do número dado; 
    o quarto dígito é o de controle calculado da seguinte forma:

    primeiro digito + 3xsegundo digito + 5xterceiro digito

    O dígito de controle é igual ao resto da divisão dessa soma por 7.
*}

Program L1E39;
Var Numero, Aux, Controle:LongInt;

Begin
    Read(Numero);

    Aux:= Numero;
    Controle:= 0;

    Controle:= Controle + (Aux div 100);
    Aux:= Aux mod 100;
    Controle:= Controle + 3*(Aux div 10);

    Aux:= Aux mod 10;
    Controle:= Controle + 5*Aux;

    Controle:= Controle mod 7;

    Numero:= Numero*10 + Controle;
    Writeln(Numero);
End.

