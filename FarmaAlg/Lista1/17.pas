{*
Faça um programa Pascal que leia um número inteiro do teclado e indique se ele está compreendido entre 20 e 
90 ou não (20 e 90 não estão na faixa de valores).
*}

Program L1E17;
Var N:LongInt;

Begin
  Read(N);
  If (20 < N) And (N < 90)
    Then Writeln('SIM')
    Else Writeln('NAO');
End.

