{*
Faça um programa que leia uma sequência de inteiros terminada em zero e armazene esta sequência em u
vetor. O zero não deve ser processado pois serve para marcar o final da entrada de dados. Em seguida o
programa deve ler uma outra sequência de inteiros também terminada em zero e, para cada valor lido, o
programa deve dizer qual a posição do valor dentro do vetor, caso o valor exista no vetor, ou 0 (zero) caso não
exista. Esta segunda sequência não precisa ser armazenada em vetores. Use ao máximo funções e
procedimentos apropriados.
*}

Program L7E4;
Const Min = 1; Max = 200;
Label Enche, Checa, Saida, TapaBuraco;
Type Vetor = Array[Min..Max] Of LongInt;
Var N, I, J:LongInt;
    V:Vetor;


Begin
    I:= 0;

    (* Experimentando Coisa Novas - Loops Com Goto *)
    Read(N);
    If N = 0 Then Begin
        Writeln('vetor vazio');
        Goto Saida;
    End Else Goto TapaBuraco;

    Enche:  Read(N); TapaBuraco:
            If N <> 0 Then Begin
                I+= 1;
                V[I]:= N;
                Goto Enche;
            End;

    Checa:  Read(N);
            If N <> 0 Then Begin
                For J:= 1 To I Do
                Begin
                    If V[J] = N Then Begin
                        Writeln(J);
                        Goto Checa;
                    End;
                End;
                Writeln(0);
                Goto Checa;
            End;

    Saida: 
End.

