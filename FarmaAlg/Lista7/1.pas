{*
Faça um programa que leia uma sequência de códigos de operação e valor, onde o código de operação é um
inteiro com os seguintes valores:

0 (zero): fim
1 (um): inserção
2 (dois): remoção

O valor lido é um número real que deve ser inserido em um vetor
(caso a operação seja 1), ou removido do vetor (caso a operação seja 2).
As inserções no vetor devem ser realizadas de forma que o vetor esteja sempre ordenado. O programa deve
imprimir todos os vetores resultantes de cada operação e ao final deve imprimir o vetor resultante. Imprima os
valores reais sempre com uma casa decimal. Caso o vetor esteja vazio, imprima a mensagem "vazio".

Detalhamento:

A quantidade máxima de valores que pode ser inserida é 200;

Se a quantidade máxima for ultrapassada o programa deve dar uma mensagem de erro (imprima a mensagem "erro");

Se for requisitada a remoção de um número não existente o programa deve dar uma mensagem de erro (imprima "erro");

Se o código de operação for inválido o programa deve continuar lendo um novo código até que ele seja 0 (zero),
1 (um) ou 2 (dois).

Use ao máximo funções e procedimentos apropriados.
*}

(*Nao gostei muito da maneira que o FarmaAlg cobra este exercicio...
Pra facilitar, os Procedures Insere e Remove vao fugir um pouco do escopo e tratar as excecoes /
Imprimir a estrutura...*)
(*O Sentinela do Insere quebra no teste...*)

Program L7E1;
Const Min = 1; Max = 200; 
Type Vetor = Array[Min..Max+1] Of Real; (*Sentinela*)
Type Estado = (Inserindo = 1, Removendo);
Var V:Vetor;
    Head, Op:LongInt;
    N:Real;

Procedure Imprime(Var V:Vetor; Head:LongInt);
Var I:LongInt;
Begin
    If Head = 0 Then Writeln('vazio')
    Else Begin
        For I:= 1 To Head Do
            Write(V[I]:0:1, ' ');
        Writeln();
    End;
End;

(*Insertion Sort*)
Procedure Arrasta(Var V:Vetor; Point, Head, Direcao:LongInt);
Var I:LongInt;
Begin

    If Direcao = 1 Then (*Arrasta para Direita*)
    Begin
        For I:= Head Downto Point Do
            V[I+1]:= V[I];
    End
    Else (*Arrasta para Esquerda*)
    Begin
       For I:=Point to Head Do 
            V[I]:= V[I+1];
    End;

End;

Procedure Insere(Var V:Vetor; Var Head:LongInt; Valor:Real);
Label Saida;
Var I:LongInt;
Begin
    If Head = Max Then
    Begin
        Writeln('erro'); 
        Goto Saida;
    End;
    
    V[Head+1]:= Valor; (*Sentinela*)
    I:= 1;
    While (V[I] <= Valor) And (I < Head+1) Do I:= I + 1; (*Sentinela ta quebrado no FarmaAlg*)
    Arrasta(V, I, Head, 1);
    V[I]:= Valor;

    Head:= Head + 1;
    Imprime(V, Head);
    Saida:
End;

Procedure Remove(Var V:Vetor; Var Head:LongInt; Valor:Real);
Label Saida;
Var I:LongInt;
Begin;
    If Head <= 0 Then
    Begin
        Writeln('erro');
        Goto Saida;
    End;

    V[Head+1]:= Valor;
    I:= 1;
    While V[I] <> Valor Do I:= I + 1;
    Arrasta(V, I, Head, -1);

    If I < Head+1 Then
    Begin
        Head:= Head - 1; (*Se o valor esta no vetor*)
        Imprime(V, Head);
    End
    Else Writeln('erro');

    Saida:
End;

(*Programa Principal*)
Begin
    Head:= 0;
    Repeat
        Read(Op);
        Case (Estado(Op)) Of
            Inserindo:
            Begin
                Read(N);
                Insere(V, Head, N);
            End; 
            Removendo:
            Begin
                Read(N);
                Remove(V, Head, N);
            End;
        End;
    Until Op = 0;
    Imprime(V, Head);
End.

