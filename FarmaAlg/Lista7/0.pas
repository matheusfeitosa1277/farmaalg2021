{*
Faça um programa que leia um número inteiro n (0 <= n <= 200) e em seguida leia uma sequência de n valores
reais e os insira em um vetor de reais.
O programa deve imprimir na saída o valor absoluto da divisão da soma dos valores positivos que estão em
posições pares pela soma dos valores
negativos que estão nas posições ímpares. Cuidado com divisões por zero.
*}


(*Nao precisava de Vetor, mas, segue que *)
Program L7E0;
Label Saida;
Const Min = 1; Max = 200;
Type Vetor = Array[Min..Max] Of Real;
Var v:Vetor; Impar, Par:Real;
    N, I:LongInt;
    
Begin
    Par:= 0;
    Impar:= 0;

    Read(N);
    If N <= 0 Then 
    Begin
        Writeln('vetor vazio');
        Goto Saida;
    End;

    For I:= 1 To N Do Read(v[I]);


    I:= 1;
    While I <= N Do
    Begin
       If v[I] < 0 Then Impar:= Impar + v[I]; 
       I:= I+2;
    End;

    I:= 2;
    While I <= N Do
    Begin
        If v[I] > 0 Then Par:= Par + v[I];
        I:= I+2;
    End;

    
    If Impar <> 0
        Then Writeln(Par/Impar:0:2)
        Else Writeln('divisao por zero');
        
    Saida:
End.

