{*
Faça um programa que leia um inteiro positivo n e em seguida leia n valores inteiros quaisquer e imprima "sim"
se o vetor estiver ordenado em ordem crescente e "nao" em caso contrário. Em qualquer caso, o programa também deve
imprimir o vetor, mas na ordem inversa com relação à leitura. Use funções e procedimentos apropriados.
*}

Program L7E2;
Label Saida;
Const Min = 1; Mx = 200;
Type Vetor = Array[Min..Mx] Of LongInt;
Var N, I, Max:LongInt;
    V: Vetor;

Begin
    Read(N);
    If N <= 0 Then
    Begin
        Writeln('vetor vazio');
        Goto Saida;
    End;

    For I:= 1 To N Do
        Read(V[I]);
    
    Max:= V[1];
    I:= 1;
    While I < N+1 Do
    Begin
        If V[I] >= Max 
            Then Max:= V[I]
            Else Break;
        I:= I + 1;
    End;

    If I > N 
        Then Writeln('sim')
        Else Writeln('nao');
    
    For I:= N Downto 1 Do
        Write(V[I], ' ');
    
    Saida:
End.

