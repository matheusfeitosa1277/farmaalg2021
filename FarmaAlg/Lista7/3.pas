{*
Faça um programa que leia um número inteiro positivo n e em seguida leia uma sequência de n números inteiros
quaisquer. Seu programa deve imprimir os números distintos (na mesma ordem relativa entre eles) e em seguida o 
número de vezes que cada um deles ocorre na mesma. Use ao máximo funções e procedimentos adequados.
Dica: você pode usar vetores auxiliares para ajudar a verificar e contar as ocorrências.
*}

(* Memoria dinamica parece uma boa *)
(* Descobri agr que pascal aceita += ... *)

Program L7E3;
Const Min = 1; Max = 200;
Label Saida;
Type Vetor = Array[Min..Max] Of LongInt;
Var N, I, Dist, Aux:LongInt;
    V, Count:Vetor;

Begin
    Dist:= 0;
    Read(N);

    For I:= 1 To Max Do Count[I]:= 0;

    For I:= 1 To N Do
    Begin
        Read(Aux);
        If Count[Aux] = 0 Then Begin
            Dist+= 1;
            V[Dist]:= Aux;
        End;

        Count[Aux]+= 1;
    End;

    (* Escrita *)
    If N = 0 Then Begin
        Writeln('vetor vazio');
        Goto Saida;
    End;
    Write('a sequencia tem ', Dist);
    If Dist = 1
        Then Write(' numero distinto: ')
        Else Write(' numeros distintos: ');

    For I:= 1 To Dist Do Write(V[I], ' ');
    Writeln();

    For I:= 1 To Dist Do 
    Begin
        Write(V[I], ' ocorre ', Count[V[I]]);
        If Count[V[I]] = 1
            Then Write(' vez')
            Else Write(' vezes');
        Writeln();
    End;

    Saida:
End.







