{*
Faça um programa em Pascal que leia um inteiro positivo n, e escreva a soma dos n primeiros termos da série
abaixo:

S = 1000/1 - 997/2 + 994/3 - 991/4 + ...

Imprima a saída com duas casas decimais.
*}

Program L5E2;
Var Numerador, Denominador, N, I, S:LongInt;
    Soma:Real;

Begin
    Soma:= 0;
    Numerador:= 1000;
    Denominador:= 1;
    S:= 1;
    Read(N);
    For I:= 1 To N Do
    Begin
        Soma:= Soma + S*(Numerador/Denominador);
        Denominador:= Denominador + 1;
        Numerador:= Numerador - 3;
        S:= -S;
    End;
    Writeln(Soma:0:2);
End.

