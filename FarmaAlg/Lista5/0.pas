{*
Faça um programa em Pascal que lê do teclado um número inteiro positivo N. Depois disso, o programa deve
calcular e imprimir a soma de todas
as frações em que a soma do numerador com o denominador de cada fração seja o número N. 

Por exemplo, se N=7, o programa deve calcular a soma abaixo:
S = 1/6 + 2/5 + 3/4 + 5/2 + 6/1
*}

Program L5E0;
Var N, Numerador:LongInt; 
    Soma:Real;

Begin
    Read(N);
    Numerador:= 1;
    Soma:= 0;
    While (N>1) Do
    Begin
        N:= N-1;
        Soma:= Soma + Numerador/N;
        Numerador:= Numerador+1;
    End;
    Writeln(Soma:0:2);
End.

