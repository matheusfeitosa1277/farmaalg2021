{*
A soma infinita indicada abaixo é composta de termos em que o numerador de cada termo é a uma potência de
base 2, cujo expoente é igual à
posição do termo na série, e o denominador é o triplo da posição do termo na série:

S = 2/3 - 4/6 + 8/9 - 16/12 + 32/15 - 64/18 + ...

Observe que o sinal de soma e subtração se alterna conforme a posição do termo na série.

Faça um programa em Pascal para calcular o valor de S, considerando apenas os 10 primeiros termos da série.
Ao final, imprimir o resultado encontrado para S com duas casas após a vírgula.
*}

Program L5E5;
Var Numerador, Denominador, S, I:LongInt;
    Soma:Real;

Begin
    Soma:= 0;
    Numerador:= 2;
    Denominador:= 3;
    S:= 1;
    For I:= 1 To 10 Do
    Begin
        Soma:= Soma + S*(Numerador/Denominador);
        Numerador:= 2*Numerador;
        Denominador:= Denominador + 3;
        S:= -S;
    End;
    Writeln(Soma:0:2);
End.

