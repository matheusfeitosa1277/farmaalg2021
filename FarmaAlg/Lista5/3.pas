{*
Observe a soma infinita abaixo. Ela é formada por frações em que cada numerador é o dobro do denominador
da fração anterior e cada denominador, por sua vez, é o dobro do numerador da fração anterior (exceto a
primeira fração).

S = 1/3 + 6/2 + 4/12 + 24/8 + 16/48 + ...

Faça um programa em Pascal para calcular o valor de S, considerando apenas os 10 primeiros termos da série.
Ao final, imprimir o resultado encontrado para S com duas casas após a vírgula.
*}

Program L5E3;
Var Numerador, Denominador, Aux, I:LongInt;
    Soma:Real;

Begin
    Soma:= 0;
    Numerador:= 1;
    Denominador:= 3;
    For I:= 1 To 10 Do
    Begin
        Soma:= Soma + Numerador/Denominador;
        Aux:= Numerador;
        Numerador:= 2*Denominador;
        Denominador:= 2*Aux;
    End;
    Writeln(Soma:0:2);
End.

