{*
Observe a soma infinita abaixo. Ela eh formada por fracoes em que cada numerador eh a soma entre o numerador e o
denominador da fracao anterior e cada denominador, por sua vez, eh a soma do seu numerador com o denominador da
fracao anterior (exceto a primeira fraca).

S = 1/1 + 2/3 + 5/8 + 13/21 + 34/55 + ...

Faca um programa em Pascal que calcula o valor de S, considerando apenas os 5 primeiros termos da serie. Ao final,
imprimir o resultado encontrado para S com duas casas decimais
*}

Program L5E1;
Var Numerador, Denominador, I:LongInt;
    Soma:Real;

Begin
    Soma:= 0;
    Numerador:= 1;
    Denominador:= 1;
    For I:= 1 To 5 Do
    Begin
        Soma:= Soma + Numerador/Denominador;
        Numerador:= Numerador + Denominador;
        Denominador:= Numerador + Denominador;
    End;
    Writeln(Soma:0:2);
End.

