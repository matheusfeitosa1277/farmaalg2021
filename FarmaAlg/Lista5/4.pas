{*
Observe a soma infinita abaixo. Ela é formada por frações em que o numerador e o denominador são os valores
sucessores dos valores do
numerador e do denominador da frações anterior, porém, alternadamente invertidos (exceto a primeira fração).

S = 1/2 + 4/3 + 5/6 + 8/7 + 9/10 + 12/11 + ...

Faça um programa em Pascal para calcular o valor de S, considerando apenas os 10 primeiros termos da série. 
Ao final, imprimir o resultado encontrado para S com 2 casas após a vírgula.
*}

Program L5E4;
Var Numerador, Denominador, I:LongInt;
    Soma:Real;

Begin
    Soma:= 0;
    Numerador:= 1;
    Denominador:= 2;
    For I:= 1 To 10 Do
    Begin
        Soma:= Soma + Numerador/Denominador;
        If (I mod 2 = 0) Then
        Begin
            Numerador:= Numerador + 1;
            Denominador:= Denominador + 3;
        End
        Else Begin
           Numerador:= Numerador + 3;
           Denominador:= Denominador + 1;
        End;
    End;
    Writeln(Soma:0:2);
End.

