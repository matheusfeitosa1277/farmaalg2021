{*
A soma infinita indicada abaixo é composta de termos em que o numerador de cada termo é a uma potência de
base 3 cujo expoente está relacionado à posição do termo na série, e o denominador é o dobro do valor da 
posição do termo na série

S = -1/2 + 3/4 - 9/6 + 27/8 - 81/10 + 243/12 - ...

Observe que o sinal de soma e subtração se alterna conforme a posição do termo na série.

Faça um programa em Pascal para calcular o valor de S, considerando apenas os 10 primeiros termos da série.
Ao final, imprimir o resultado encontrado para S, com duas casas decimais.
*}

Program L5E6;
Var Numerador, Denominador, S, I:LongInt;
    Soma:Real;
 
Begin
    Soma:= 0;
    Numerador:= 1;
    Denominador:= 2;
    S:= -1;
    For I:= 1 To 10 Do
    Begin
        Soma:= Soma + S*(Numerador/Denominador);
        Numerador:= 3*Numerador;
        Denominador:= Denominador + 2;
        S:= -S;
    End;
    Writeln(Soma:0:2);
End.

