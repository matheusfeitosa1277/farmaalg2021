{*
Faça um programa Pascal que leia do teclado um inteiro positivo N diferente de zero e calcule a soma dos N 
primeiros cubos.

    = 1^3 + 2^3 + ... + N^3

Imprima "erro" caso o número lido não satisfaça as condições. Em caso contrário imprima o resultado do 
cálculo.
*}

Program L3E6;
Var N, Soma:LongInt;

Begin
    Soma:= 0;
    Read(N);

    If N > 0 Then
    Begin
        Repeat
        Soma:= Soma + N*N*N;
        N:= N - 1;
        Until N < 1;

        Writeln(Soma);
    End
    Else Writeln('ERRO')
End.

