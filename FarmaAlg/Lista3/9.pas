{*
 um programa Pascal que receba uma sequência de números reais terminada em zero representando
 o saldo bancário de alguns clientes de um banco e imprima aqueles que são negativos. O valor zero serve para
 indicar o final da entrada de dados e não deve ser processado.
*}

Program L3E9;
Var N:Real;

Begin
    Repeat
        Read(N);
        If N < 0 Then Writeln(N:0:2);
    Until N = 0;
End.

