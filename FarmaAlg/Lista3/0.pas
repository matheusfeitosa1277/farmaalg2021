{*
Faça um programa Pascal que calcule o valor da soma dos quadrados dos primeiros 50 inteiros positivos não 
nulos e imprima o resultado do cálculo na tela. 
Observe que este programa não tem entrada, apenas saída.
*}

Program L3E0;
Var I, S:LongInt;

Begin
    For I:= 1 To 50 Do
        S:= S + I*I; 

    Writeln(S);
End.

