{*
Uma agência governamental deseja conhecer a distribuição da população do país por faixa salarial. Para isto, 
coletou dados do último censo realizado e criou um arquivo contendo, em cada linha, o salário de um cidadão 
particular. Os salários variam de zero a 19.000,00 unidades da moeda local.

Considere o salário mínimo igual a 450,00 unidades da moeda local.
As faixas salariais de interesse são as seguintes:
    
    de 0 a 3 salários mínimos
    de 4 a 9 salários mínimos
    de 10 a 20 salários mínimos
    acima de 20 salários mínimos.

Faça um programa Pascal que leia uma sequência de números reais, que será terminado em zero. O zero não 
deve ser processado e serve para terminar o programa. O programa deve imprimir na tela os percentuais da 
população para cada faixa salarial de interesse.
*}

Program L3E1;
Const SMinimo = 450; Td = 3*SMinimo; Tc = 9*SMinimo; Tb = 20*SMinimo;
Var A, B, C, D:LongInt;
    I, Total:Real;

Begin
    A:= 0; B:= 0; C:= 0; D:= 0; Total:= 0;

    Read(I);
    While I <> 0 Do
    Begin
        If I <= Td Then D:= D + 1
        Else If I <= Tc Then C:= C + 1
        Else If I <= Tb Then B:= B + 1
        Else A:= A + 1;

        Total:= Total + 1;
        Read(I);
    End;
    Total:= Total / 100;
    Writeln(D/Total:0:2); Writeln(C/Total:0:2); Writeln(B/Total:0:2); Writeln(A/Total:0:2);
End.

