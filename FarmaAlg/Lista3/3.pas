{*
Faça um programa Pascal que leia do teclado um número inteiro m e em seguida uma sequência de n números
reais. Imprima a média aritmética inteira deles.

    (N1 + N2 + ... + Nm) / m

*}

Program L3E3;
Var Vezes, Num, Soma, I:LongInt;

Begin
    Soma:= 0;
    Read(Vezes);

    For I:= 1 To Vezes Do
    Begin
        Read(Num);
        Soma:= Soma + Num;
    End;

    Writeln(Soma Div Vezes);

End.

