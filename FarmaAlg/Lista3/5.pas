{*
Faça um programa Pascal que leia do teclado um conjunto de números onde cada linha contém dois valores
numéricos sendo o primeiro do tipo real e o segundo do tipo inteiro. O segundo valor é o peso atribuído ao
primeiro valor. O programa deve calcular e imprimir a média ponderada dos diversos valores lidos. A última
linha de dados contém os números zero. Esta linha não deve ser considerada no cálculo da média e serve
apenas para marcar o final da entrada de dados.

Isto é, calcular o seguinte, supondo que m linhas foram digitadas:

    (N1 * P1 + N2 * P2 + ... + Nm * Pm) / (P1 + P2 + ... + Pm)

Imprima o resultado com duas casas decimais.
*}

Program L3E5;
Var N, P, Soma, Divisor:LongInt;

Begin
    Soma:= 0;
    Divisor:= 0;
    Repeat
        Read(N, P);
        Soma:= Soma + N*P;
        Divisor:= Divisor + P;
    Until  (N = 0) And (P = 0);

    If Divisor <> 0 Then Writeln(Soma/Divisor:0:2);
   

End.

