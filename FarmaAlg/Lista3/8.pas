{*
Faça um programa Pascal que leia um número inteiro positivo N do teclado. Depois disso, o programa deve 
calcular e imprimir todos os arranjos de dois números inteiros positivos A, B, ambos menores que N, de forma 
que quando somados (A + B), resultam no número N. Cada arranjo A, B deve ser impresso em uma linha de 
saída.
*}

Program L3E8;
Var N, Aux:LongInt;

Begin
    Read(N);

    For Aux:= 1 To N-1 Do
    Begin
        N:= N - 1;
        Writeln(Aux, ' ', N);
    End;

End.

