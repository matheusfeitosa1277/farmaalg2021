{*
Faça um programa Pascal que leia dois números inteiros, um n e outro m. Seu programa deve imprimir a soma 
de todos os números pares entre n e m; sendo que, n e m não devem ser incluídos na soma.
*}

Program L3E12;
Var N, M, Soma:LongInt;

Begin
    Soma:= 0;
    Read(N, M);

    If (N And 1 = 1) 
        Then N:= N + 1 (* Se Impar, Soma 1 *)
        Else N:= N + 2;

    While N < M Do
    Begin
        Soma:= Soma + N; 
        N:= N + 2;
    End;
    Writeln(Soma);

End.

