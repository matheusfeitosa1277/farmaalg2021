{*
Faça um programa Pascal que leia do teclado dois números positivos ímpares A e B onde (A <= B) e calcule o 
produto dos números ímpares de A até B
Isto é, calcule:

    A * (A + 2) * (A + 4) * ... * B

Imprima "erro" caso o número lido não satisfaça as condições. Caso contrário imprima o resultado do cálculo.
*}

Program L3E4;
Var A, B, Soma:LongInt;

Begin
    Read(A, B);
    Soma:= A;

    If (A And 1 = 1) And (A > 0) Then (* Ultimo Bit Eh 1 <=> Impar *)
    Begin
        While A < B Do
        Begin
            A:= A + 2;
            Soma:= Soma * A;
        End;
        Writeln(Soma);
    End
    Else Writeln('EROO');

End.

