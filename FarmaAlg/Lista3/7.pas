{*
Faça um programa Pascal que leia uma sequência de números reais terminada em 0 que representam a medida 
dos lados de um polígono e imprima "SIM" se ele é um polígono regular (todos os seus lados iguais) e "NAO" 
caso contrário.
Note que um polígono precisa ter ao menos 3 lados para ser um polígono O número zero serve para indicar o 
final da entrada de dados e não deve ser processado.
*}

Program L3E7;
Var Medida, Aux:Real;
    Lados:LongInt;

Label Saida;

Begin
    Lados:= 0;
    Read(Aux); Medida:= Aux;

    While Medida <> 0 Do
    Begin
        If Medida <> Aux Then Goto Saida;

        Lados:= Lados + 1;
        Read(Medida);
    End;

    If Lados < 3 
        Then Saida: Writeln('NAO')
        Else Writeln('SIM');

End.

