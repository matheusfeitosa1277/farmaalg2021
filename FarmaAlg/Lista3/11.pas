{*
Faça um programa Pascal que leia dois números n e outro m (0 <= n <= 9) e conte quantos dígitos n existem em 
m. Se não existir nenhum dígito correspondente, a mensagem "NAO" deve ser exibida. Caso contrário imprima 
o resultado do seu cálculo.
*}

Program L3E11;
Var N, M, Ocorrencias:LongInt;

Begin
    Ocorrencias:= 0;
    Read(N, M);
    While M <> 0 Do
    Begin
       If M Mod 10 = N Then Ocorrencias:= Ocorrencias + 1;
       M:= M Div 10;
    End;

    If Ocorrencias > 0
        Then Writeln(Ocorrencias)
        Else Writeln('NAO');

End.

