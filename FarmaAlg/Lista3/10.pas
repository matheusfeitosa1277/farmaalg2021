{*
Faça um programa Pascal que leia do teclado uma quantidade arbitrária de números inteiros positivos 
terminada em zero e identifique o maior múltiplo de 7 (sete) entre esses números. Depois da leitura dos dados, 
o maior múltiplo de 7 encontrado deve ser impresso. O número zero serve para indicar o final da entrada e não 
deverá ser processado.
*}

Program L3E10;
Var N, Max:LongInt;

Begin
    Max:= 0;

    Repeat
        Read(N);
        If (N > Max) And (N Mod 7 = 0) Then Max:= N;
    Until N = 0;

    Writeln(Max);

End.

