{*
Faça um programa em Free Pascal que leia dois inteiros positivos m e n, sendo 1 ≤ m, n ≤ 100, e uma matriz A
m×n . O programa deve imprimir o números de linhas e o número de colunas que são nulas, ou seja, quando 
todos os elementos de uma linha ou coluna são iguais a 0 (zero).
Nos casos de teste cada elemento x da matriz A é definido por 0 ≤ x ≤ 100
*}

(*Implementacao Ingenua*)
(*Talvez de pra usar BackTracking, era bom revisar isso*)
Program L9E1;
Uses MatrizUtilitarios In '../../Pascal/Units/MatrizUtilitarios.pas';
Type Vetor = Array [1..100] Of Integer;
Var Matrix:Matriz; 
    M, N, I, J, L, C:LongInt;
    Coluna, Linha:Vetor;

Begin
    Read(M, N);
    LeMatriz(Matrix, M, N);
    L:= 0; C:= 0;

    For I:= 1 To M Do
        Linha[I]:= 1;
    For I:=1 To N Do
        Coluna[I]:= 1;


    For I:= 1 To M Do
    Begin
        For J:= 1 To N Do
        Begin
            If Matrix[I, J] <> 0 Then
            Begin
                Linha[I]:= 0;
                Coluna[J]:= 0;
            End;
        End;
    End;

    For I:= 1 To M Do
        If Linha[I] = 1 Then L:= L+1;


    For I:= 1 To N Do
        If Coluna[I] = 1 Then C:= C+1;

    Writeln('linhas: ', L);
    Writeln('colunas: ', C);
End.

