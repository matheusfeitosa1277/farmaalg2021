{*
Faça um programa em Free Pascal que leia dois inteiros positivos m e n, sendo 1 ≤ m, n ≤ 100, e uma matriz A 
m×n . O programa deve imprimir “sim” se há elementos repetidos na matriz A, caso contrário deve imprimir 
“nao”.
Nos casos de teste cada elemento x da matriz A é definido por 1 ≤ x ≤ 1000.

PS: Seu programa deve encerrar a execução assim que descobrir se a propriedade definida foi atendida ou não. 
*}

Program L9E2;
Uses MatrizUtilitarios In '../../Pascal/Units/MatrizUtilitarios.pas';
Const VetMax = 1000;
Type Vetor = Array [1..VetMax] Of Integer;
Label Saida;
Var Matrix:Matriz;
    M, N, I, J, Aux:LongInt;
    Repetidos:Vetor;

Begin
    For I:=1 To VetMax Do
        Repetidos[I]:= 0;

    Read(M, N);
    LeMatriz(Matrix, M, N);

    For I:= 1 To M Do
    Begin
        For J:= 1 To N Do
        Begin
            Aux:= Matrix[I, J];
            If Repetidos[Aux] = 1
            Then Begin
                Writeln('sim');
                Goto Saida;
            End
            Else Repetidos[Aux]:= 1;
        End; 
    End;
    Writeln('nao'); 
    Saida:
End.

