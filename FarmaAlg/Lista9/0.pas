{*
Uma matriz quadrada A n×n é considerada triangular quando os elementos que estão acima da sua diagonal
principal são todos  nulos (matriz triangular inferior) ou quando os elementos que estão abaixo de sua diagonal
principal são todos nulos (matriz triangular superior). Vejamos dois exemplos de matrizes triangulares:

    9 8 7 6
    0 6 7 3
    0 0 2 5
    0 0 0 1

Matriz triangular Superior

    6 0 0 0
    3 0 0 0
    0 4 7 0
    7 5 2 1

Matriz triangular Inferiror

Faça um programa em Free Pascal que leia um inteiro positivo n, sendo 1 ≤ m, n ≤ 100, e uma matriz inteira A
n×n . O programa deve imprimir “sim” caso a matriz A seja triangular, caso contrário deve imprimir “nao”. 
Nos casos de teste cada elemento x da matriz A é definido por 0 ≤ x ≤ 100.

PS: Seu programa deve encerrar a execução assim que descobrir se a propriedade definida foi atendida ou não. 

*}

Program L9E0;
Type Matriz = Array [1..100, 1..100] Of Integer;
Var N:LongInt;
    M: Matriz;

Procedure LeMatrizQuadrada(Var M:Matriz; N:Integer);
Var I, J:LongInt;
Begin
    For I:= 1 To N Do
        For J:= 1 To N Do
            Read(M[I, J]);
End;

Function ChecaMatrizTriangular(Var M:Matriz; N:Integer):Boolean;
Label Saida;
Var I, J, Aux:LongInt;
    Inf, Sup:Boolean;
Begin
    Aux:= 1;
    Inf:= True ; Sup:= True;
    For J:= 1 To N Do
    Begin
        If Inf Then Begin
            For I:= 1 To Aux - 1 Do (*Triangular Inferior*)
                If M[I, J] <> 0 Then Inf:= False;
        End;

        If Sup Then Begin
            For I:= Aux + 1 To N Do (*Triangular Superior*)
                If M[I, J] <> 0 Then Sup:= False;
        End;

        If not (Inf Or Sup) Then Goto Saida;
        Aux:= Aux + 1;
    End;

    Saida:

    ChecaMatrizTriangular:= (Inf Or Sup);
End;

Begin
    Read(N);
    LeMatrizQuadrada(M, N);
    If ChecaMatrizTriangular(M, N) 
        Then Writeln('sim')
        Else Writeln('nao');
End.

