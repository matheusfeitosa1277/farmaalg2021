{*
Faça um programa em Free Pascal que leia diversas linhas nas quais as linhas ı́mpares contém o tamanho n (0 ≤ 
n ≤ 100) da sequência de números inteiros que deve ser lida na linha subsequente. Quando uma linha ı́mpar 
contiver o valor zero significa que a entrada de dados acabou e seu programa deverá encerrar com a impressão 
de todas as sequências originais e também compactadas pela eliminação de
todos os elementos repetidos de cada sequência. 
No exemplo abaixo, O significa a sequência original e C a sequência compactada.
*}

(* Busca Linear Preguicosa *)
(* Nao ta muito elegante ... *)

Program L8E1;
Const Min = 1; Max = 100;
Type Vetor = Array [Min..Max] Of LongInt;
Label Leitura, Compacta;
Var N, I, J, Aux:LongInt;
    V:Vetor;

Begin

Leitura:
    Read(N);
    If N <> 0 Then Begin (* Condicao de Parada *)
        Write('O: '); 
        For I:= 1 To N Do
        Begin
            Read(V[I]);
            Write(V[I], ' ');
        End;
        Writeln();

        Write('C: ');
        For I:= 1 To N Do
        Begin
            Aux:= V[I];
            For J:= 1 To I-1 Do
                If V[J] = Aux Then Goto Compacta;  (* Repetido *)
               
            Write(Aux, ' ');

            Compacta:
        End;
        Writeln();

        Goto Leitura;
    End;

End.

