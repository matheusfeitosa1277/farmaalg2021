{*
Faça um programa em Free Pascal que leia um número natural 0 < n ≤ 100 e em seguida leia uma sequência de n
números também naturais. Seu programa deve verificar se existem duas subsequências iguais nesta sequência
com tamanho pelo menos 2. O tamanho da sequência encontrada deverá ser máximo, se ela existir. Caso exista,
seu programa deve imprimir o valor do ı́ndice i e do tamanho máximo da sequência m, nesta ordem, onde i é a
primeira ocorrência da sequência que possui uma cópia na sequência original e m é o tamanho desta sequência
que se repete. Caso contrário seu programa deve imprimir "nenhuma".

Os casos de teste não conterão entradas com mais de uma subsequência igual.

Sugestão:
Use como base para sua implementação a seguinte estrutura inicial de programa, que contém o programa
principal e algumas funções e procedimentos que visam facilitar o seu trabalho. Evidentemente você pode
ignorar esta sugestão. Caso aceite, você deve implementar as funções e procedimentos, o programa principal
não deveria ter que ser alterado, a princı́pio. Você pode também decidir usar mais funções e procedimentos
caso perceba que seu programa ficará mais legı́vel.
*}

(* Precisa ser refatorado urgentementes *)

Program L8E3;
Const Min = 0; Max = 100;
Type Vetor = Array [Min..Max] Of LongInt;
Var N, Posi, TamSubs:LongInt;
    V:Vetor;

Procedure LeVetor(Var V:Vetor; N:LongInt);
Var I:LongInt;
Begin
    For I:= 0 To N-1 Do
        Read(V[I]);
End;

Procedure CopiaSubVetor(Var V, Copia:Vetor; Ini, Fim:LongInt);
Var I:LongInt;
Begin
    For I:= 0 To (Fim - Ini) Do
        Copia[I]:= V[I + Ini];
        
End;

Function TemSubsIguais(Var V:Vetor; N, TamSeg:LongInt): LongInt;
Label Return;
Var I, J, K, IMax:LongInt;
    Aux:Vetor;
    Valida:Boolean;
Begin
    IMax:= N - 2*TamSeg + 2; (* Numero maximo de Segmentos *)

    For J:= 0 To IMax Do (* Todas As Subsequencias Possiveis, Iniciando em J *)
    Begin
        CopiaSubVetor(V, Aux, J, J +  TamSeg - 1);

        (* Se nao quiser sequencias que comecao e acabam com o mesmo numero use K = 1 *)
        (* Ex FarmaAlg [5 4 6 [5] 4 6 5] *)
        For K:= 0 To N -(J + TamSeg - 1) Do 
        Begin
            Valida:= True;

            For I:= 0 To TamSeg - 1 Do 
            Begin
                If Aux[I] <> V[TamSeg - 1 + J + I + K] Then Begin (* Subs Invalida *)
                    Valida:= False;
                    Break;
                End
            End;

            If Valida Then Begin (* Achou uma SubSequencia *)
                TemSubsIguais:= J + 1;
                Goto Return;
            End;
        End;
    End;

    TemSubsIguais:= 0;
    Return:

End;

Begin
    Read(N);
    LeVetor(V, N);

    Posi:= 0;
    TamSubs:= N Div 2; (* Inicia com maior valor possivel *)
    While (Posi = 0) And (TamSubs >= 2) Do
    Begin
        Posi:= TemSubsIguais(V, N, TamSubs);
        TamSubs -= 1;
    End;

    If Posi > 0 
        Then Writeln(Posi, ' ', TamSubs+1)
        Else Writeln('nenhuma');

End.


