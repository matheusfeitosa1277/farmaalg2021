{*
Fazer um programa em Free Pascal que leia do teclado dois números naturais 0 < n, m ≤ 100. Em seguida, leia
duas sequências de respectivamente n e m números também naturais, sendo garantidamente m < n.
Seu programa deve determinar quantas vezes a segunda sequência ocorre na primeira.
*}

Program L8E2;
Const Min = 1; Max = 100;
Type Vetor = Array[Min..Max] Of LongInt;
Var N, M, Count, I, J:LongInt;
    Sequencia, Sub:Vetor;
    Seq:Boolean;

Begin
    Count:= 0; Seq:= True;
    Read(N, M);

    For I:= 1 To N Do Read(Sequencia[I]);
    For I:= 1 To M Do Read(Sub[I]);


    I:= 0;
    While I <= N Do
    Begin 
        For J:= 1 To M Do 
        Begin
            I += 1;
            If Sequencia[I] <> Sub[J] Then Begin
                Seq:= False;
                Break;
            End;
        End;

        If Seq Then Begin
            Count += 1;
            I -= 1; (* Checa se a Possivel Sub Comecam e Terminam com o mesmo numero *)
        End;

        Seq:= True;
    End;

    Writeln(Count);
End.

