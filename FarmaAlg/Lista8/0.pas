{*
Faça um programa em Free Pascal que leia um número natural 0 < n ≤ 100 e em seguida leia uma sequência de
n números inteiros. Seu programa deve determinar o valor do subsequência que maximize a soma dos seus
elementos.

Observação 2: Idealmente, seu programa deve fazer o menor número possı́vel de somas dos elementos dos
vetores, embora o FARMA-ALG apenas checa se sua resposta está correta. Você consegue fazer um programa
que nunca some duas vezes uma sequência que já foi somada antes? Teste isso fora do FARMA-ALG. 
*}


(* Tem espaco pra otimizacao aqui... *)
(* No enviado pro FarmaAlg Segmento = Acumulador *)

Program L8E0;
Const Min = 1; Maxi = 200;
Type Vetor = Array[Min..Maxi] Of LongInt;
Var N, I, Aux, Neg, Segmento, Max:LongInt; 

Begin
    (* Subproblema dos segmentos crescentes e decrescentes *)
    Segmento:= 0; Neg:= 0; Max:= 0;
    I:= 1;
    Read(N);
    While I <= N Do
    Begin
        Read(Aux);
        If Aux >= 0 Then Begin (* Inicio de um Segmento Crescente *)
            If Neg > 0 Then Segmento -= Neg;

            Segmento += Aux;
            If Segmento > Max Then Max:= Segmento;
        End
        Else Neg -= Aux;

        If Neg > Segmento Then Begin (* Nao Compensa Somar O Segmento Decrescente *)
            Segmento:= 0; 
            Neg:= 0;
        End;

        I += 1;
    End;

    Writeln(Max);
End.

