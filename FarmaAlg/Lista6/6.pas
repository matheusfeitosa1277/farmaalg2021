{*
Faça um programa que receba os valores antigo e atual de um produto. Use uma função que determine o 
percentual de acréscimo entre esses valores. O resultado deverá ser mostrado pelo programa principal.
*}

Program L6E6;
Var Antigo, Atual:Real;

Function AumentoPercentual(Antigo, Atual:Real):Real;
Begin
    AumentoPercentual:= Atual/Antigo - 1;
End;

Begin
    Read(Antigo, Atual);
    
    While (Antigo <> 0) Or (Atual <> 0)  Do
    Begin
        Writeln(AumentoPercentual(Antigo, Atual):0:2);
        Read(Antigo, Atual);
    End;
End.

