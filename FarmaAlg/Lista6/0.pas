{*
Faça duas funções para calcular o seno e o cosseno de um número real lido do teclado representando um
ângulo em radianos. Calcule até o sexto termo das séries. Faça uma terceira função que calcule a tangente deste
mesmo ângulo lido. Esta função deve retornar true caso a tangente exista e false em caso contrário. Ao mesmo 
tempo, a função tangente deve retornar o valor da tangente do ângulo caso ele exista, senão deve imprimir uma
mensagem adequada. Teste suas funções usando o código abaixo:
*}

Program L6E0;  
Var Angulo, Tg: Real;

{*
    Definicao de Seno por Series de Potencias - Polinomio de Taylor
    sin(x) = x - x^3/3! + x^5/5! - x^7/7! + ...
*}
Function Seno(Angulo:Real):Real;
Var Denominador, I, S:LongInt;
    Numerador, Soma:Real;
Begin
    Soma:= 0;
    (*Primeiro Termo - a0*)
    Numerador:= Angulo;
    Denominador:= 1;
    S:= 1;
    For I:= 1 To 6 Do
    Begin
        Soma:= Soma + S*(Numerador/Denominador);
        (*Calculo aI*)
        Numerador:= Numerador*Angulo*Angulo;
        Denominador:= Denominador*(2*I)*(2*I+1) ;
        S:= -S;
    End;
    Seno:= Soma;
End;

{*
    Definicao de Cosseno por Series de Potencias - Polinomio de Taylor
    cos(x) = 1 - x^2/2! + x^4/4! - x^6/6! + ...
*}

Function Coss(Angulo:Real):Real;
Var I, S:LongInt;
    Numerador, Denominador, Soma:Real;
Begin
    Soma:= 0;
    (* Primeiro Termo - a0 *)
    Numerador:= 1;
    Denominador:= 1;
    S:= 1;
    For I:= 1 To 6 Do
    Begin
        Soma:= Soma + S*(Numerador/Denominador);
        (*Calculo aI*)
        Numerador:= Numerador*Angulo*Angulo;
        Denominador:= Denominador*(2*I)*(2*I-1);
        S:= -S;
    End;
    Coss:= Soma;
End;

{*
    Definicao de Tangente de um angulo
    tan(x) = sin(x)/cos(x)

    Se cos(x) = 0, mutliplos de 90 graus ou pi/2 radianos, teremos uma
    Indeterminacao
*}

Function ExisteTangente(Angulo:Real; Var Tan:Real):Boolean;
Var Sen, Cosse:Real;
Begin
    Cosse:= Coss(Angulo);
    If abs(Cosse) > 0.000001 Then 
    Begin
        Sen:= Seno(Angulo);
        Tan:= Sen/Cosse;
        ExisteTangente:= True;
    End
    Else
        ExisteTangente:= False;
End;

{*
Programa Principal
Place Holder do FarmaAlg
*}
Begin
    Read(Angulo);
    If ExisteTangente(Angulo, Tg) Then
        Writeln(Tg:0:3)
    Else
        Writeln('Nao Existe Tangente de ', Angulo:0:5);
End.

