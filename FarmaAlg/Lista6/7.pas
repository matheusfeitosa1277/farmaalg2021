{*
Faça uma função que receba como parâmetros dois números inteiros não nulos e retorne true se um for o 
contrário do outro e false em caso contrário. Teste sua função usando este código:
*}

Program L6E7;
Var N, M:LongInt;

Function Contrario(N, M:LongInt):Boolean;
Var Soma:LongInt;
Begin
    Soma:= 0;
    While N > 0 Do
    Begin
        Soma *= 10;
        Soma += N Mod 10;
        N:= N Div 10;
    End;

    If (Soma = M)
        Then Contrario:= True
        Else Contrario:= False;
End;

Begin
    Read(N, M);
    If Contrario(N, M) 
        Then Writeln(N, ' eh o contrario de ', M)
        Else Writeln(N, ' nao eh o contrario de ', M);
End.

