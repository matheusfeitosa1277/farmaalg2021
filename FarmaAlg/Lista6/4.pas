{*
Faça uma função que receba como parâmetros seis inteiros dia1, mes1, ano1, dia2, mes2, ano2 , todas do tipo
integer. Considerando que cada trinca de dia, mês e ano representa uma data, a função deve retornar true se a
primeira data for anterior à segunda e false caso contrário. Teste sua função usando o código abaixo.
*}

Program L6E4;
Var Dia1, Mes1, Ano1, Dia2, Mes2, Ano2:LongInt;

Function EhAnterior(Dia1, Mes1, Ano1, Dia2, Mes2, Ano2:LongInt):Boolean;
Var Primeiro, Segundo:LongInt;

Begin
    Primeiro:= Ano1*365 + Mes1*12 + Dia1; (* Condicionais sao lentas e chatas de paralelizar *)
    Segundo:= Ano2*365 + Mes2*12 + Dia2;

    If Primeiro < Segundo
        Then EhAnterior:= True
        Else EhAnterior:= False;
End;

Begin
    Read(Dia1, Mes1, Ano1, Dia2, Mes2, Ano2);
    If EhAnterior(Dia1, Mes1, Ano1, Dia2, Mes2, Ano2)
        Then Writeln('a primeira data eh anterior')
        Else Writeln('a primeira data nao eh anterior');
End.

