{*
Faça uma função que receba como parâmetro um número inteiro e retorne true se ele for primo e false em caso 
contrário. Teste sua função usando o código abaixo, que imprime todos os primos entre 1 e 10000.
*}

Program L6E3;
Var I:LongInt;

Function EhPrimo(I:LongInt):Boolean;
Var Aux:LongInt;
Begin
    EhPrimo:= True;
    
    If I Mod 2 = 0 Then Begin
        If I <> 2 Then EhPrimo:= False
    End
    Else Begin
        Aux:= 3;
        While Aux <= Sqrt(I) Do
        Begin
            If I Mod Aux = 0 Then  Begin
                EhPrimo:= False;
                Break;
            End;
            Aux:= Aux + 2;
        End;
    End;
End;

Begin
    For I:= 1 To 10000 Do
        If EhPrimo(I) Then Writeln(I);
End.

