{*
Faça uma procedure que receba como parâmetro um inteiro e retorne este número incrementado de uma
unidade. Use esta procedure para fazer funcionar o código abaixo, que imprime todos os números de 1 a 10. 
Teste sua procedure usando o código abaixo.
*}

Program L6E5;
Var N:LongInt;

Procedure Incrementa(Var N:LongInt);
Begin
    N:= N + 1;
End;

Begin
    N:= 1;

    While N <= 10 Do
    Begin
        Writeln(N);
        Incrementa(N);
    End;
End.

