{*
Faça uma função que calcule a média ponderada com pesos respectivamente 1, 2 e 3 para as provas 1, 2 e 3.
Faça também outra função que decida se um aluno foi aprovado ou reprovado, sabendo que a aprovação deve
ter a média final maior ou igual a 50. As notas das provas e a média são valores inteiros entre zero e 100. Você 
pode testar sua procedure usando o código abaixo:
*}

Program L6E8;
Var I, N, P1, P2, P3, Media:LongInt;

Function MediaPonderada(P1, P2, P3:LongInt):LongInt;
Begin
    MediaPonderada:= (P1 + 2*P2 + 3*P3) Div 6;
End;

Function Aprovado(Media:LongInt):Boolean;
Begin
    If Media >= 50
        Then Aprovado:= True
        Else Aprovado:= False;
End;

Begin
    Read(N);

    For I:= 1 To N Do
    Begin
        Read(P1, P2, P3);
        Media:= MediaPonderada(P1, P2, P3);
        If Aprovado(Media)
            Then Writeln('aluno ', I, ' aprovado com media: ', Media)
            Else Writeln('aluno ', I, ' reprovado com media: ', Media);
    End;
End.

