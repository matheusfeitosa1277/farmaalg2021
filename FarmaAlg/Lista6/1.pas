{*
Faça uma função que receba como parâmetro um número inteiro e teste se ele é um número binário. Se ele for
binário, imprima sim senão imprima nao. Teste sua função usando este código:
*}

Program L6E1;
Var N:LongInt;

Function EhBinario(N:LongInt):Boolean;
Begin
    EhBinario:= True;

    While N > 0 Do
    Begin
        If (N Mod 10) > 1  Then
        Begin
            EhBinario:= False;
            Break;
        End;
        N:= N Div 10;
    End;
End;

Begin
    Read(N);
    If EhBinario(N)
        Then Writeln('sim')
        Else Writeln('nao');
End.

