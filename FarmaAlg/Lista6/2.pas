{*
Faça uma função que receba como parâmetro um número inteiro garantidamente binário e o converta para
decimal. Teste sua função usando este código:
*}

Program L6E2;
Var N:LongInt;

Function ConverteEmDecimal(N:LongInt):LongInt;
Var Soma, Decimal:LongInt;
Begin
    Soma:= 0;
    Decimal:= 1;
    While N > 0 Do
    Begin
        If (N Mod 10) = 1 Then Soma += Decimal;
        Decimal *= 2;

        N:= N Div 10;
    End;

    ConverteEmDecimal:= Soma;
End;

Begin
    Read(N);
    Writeln(ConverteEmDecimal(N));
End.

