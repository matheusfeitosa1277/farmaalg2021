# FarmaAlg - Algoritmos e Estruturas de dados 1 - UFPR

![Quadrado Magico](https://www.inf.ufpr.br/msf21/resources/ultimoExercicio.png)

## Introdução

"O amadurecimento dos cursos técnicos e tecnológicos de desenvolvimento de software fez emergir necessidades tanto para docentes quanto para estudantes. Para os docentes, há a demanda de conhecer metodologias de ensino e de como compartilhar conhecimento e avaliar a efetividade das informações fornecidas de modo imediato, preciso e, cada vez mais, online. Do lado dos estudantes, demandam-se conteúdos alinhados com o mercado e de qualidade para auxiliar no seu desenvolvimento educacional e profissional. Diante destes principais pontos, a aplicação Farma-Alg propõe um ambiente para interação aluno-professor perante o ensino de lógica de programação e algoritmos, entregando ao aluno uma área de estudos e ao professor uma plataforma para acompanhar e mensurar a assimilação destes temas pelas suas turmas."

Müller, Júlio do Lago,

Alflen, Lia,

Sales, Rafael Zezilia Lopes.

## Resumo

Neste repositório segue a resolução de todos os 118 exercícios propostos pela plataforma, na linguagem Pascal.

Dentre os conteúdos relacionados a lógica de programação, são abordados:

-   Expressões aritméticas
    
-   Desvios condicionais
    
-   Expressões booleanas
    
-   Progressões Aritméticas e Geométricas
    
-   Cálculos com resto de divisão inteira
    
-   Repetições
    
-   Acumuladores
    
-   Séries
    
-   Funções e Procedimentos
    
-   Vetores
    
-   Matrizes
    
-   Criação de Bibliotecas
    
-   Outros
